<?php

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    name: 'app:execute-sql',
    description: 'Exécute un script SQL depuis un fichier'
)]
class ExecuteSqlCommand extends Command
{
    public function __construct(
        private Connection $connection,
        private string $projectDir
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $sqlFilePath = $this->projectDir . '/src/sql/test.sql';

        if (!file_exists($sqlFilePath)) {
            $output->writeln('<error>Fichier SQL non trouvé : ' . $sqlFilePath . '</error>');
            return Command::FAILURE;
        }

        try {
            $sql = file_get_contents($sqlFilePath);
            $this->connection->executeStatement($sql);
            $output->writeln('<info>Script SQL exécuté avec succès</info>');
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln('<error>Erreur : ' . $e->getMessage() . '</error>');
            return Command::FAILURE;
        }
    }
}
// php bin/console app:execute-sql
