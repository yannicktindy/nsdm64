<?php

namespace App\Utils;

class Image
{
    const BANNER_PATH = 'https://www.insidiome.com/build/img/mapped/banner/';
    private $banners = [
        'ban-army1.jpg',
        'ban-army2.jpg',
        'ban-bears1.jpg',
        'ban-bears2.jpg',
        'ban-bikers1.jpg',
        'ban-bikers2.jpg',
        'ban-corpo1.jpg',
        'ban-corpo2.jpg',
        'ban-daddies1.jpg',
        'ban-daddies2.jpg',
        'ban-dogs1.jpg',
        'ban-dogs2.jpg',
        'ban-gimp1.jpg',
        'ban-gimp2.jpg',
        'ban-interro1.jpg',
        'ban-interro2.jpg',
        'ban-locker1.jpg',
        'ban-locker2.jpg',
        'ban-office1.jpg',
        'ban-office2.jpg',
        'ban-old-guard1.jpg',
        'ban-old-guard2.jpg',
        'ban-sense1.jpg',
        'ban-sense2.jpg',
    ];

    public function getOneRandomBanner(): string
    {
        shuffle($this->banners); 
        $banner = array_rand($this->banners);
        return self::BANNER_PATH . $this->banners[$banner];
    }

}