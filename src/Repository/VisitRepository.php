<?php

namespace App\Repository;

use App\Entity\Visit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Visit>
 *
 * @method Visit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Visit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Visit[]    findAll()
 * @method Visit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Visit::class);
    }

    public function save(Visit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Visit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function todayVisits(): int
    {
        $todayVisits = $this->createQueryBuilder('v')
            ->where('v.visitDate = :today')
            ->setParameter('today', new \DateTime('today'))
            ->getQuery()
            ->getResult();

        return count($todayVisits);
    }

    public function lastSevenDaysVisits(): int
    {
        $lastSevenDaysVisits = $this->createQueryBuilder('v')
            ->where('v.visitDate >= :weekAgo')
            ->setParameter('weekAgo', new \DateTime('7 days ago'))
            ->getQuery()
            ->getResult();

        return count($lastSevenDaysVisits);
    }




    public function countEachDayVisitsForLastSevenDays(): array
    {
        $dailyVisits = $this->createQueryBuilder('v')
            ->where('v.visitDate >= :weekAgo')
            ->setParameter('weekAgo', new \DateTime('7 days ago'))
            // ->groupBy('DAY(v.visitDate)')
            // ->orderBy('v.visitDate', 'DESC')
            ->getQuery()
            ->getResult();


        return $dailyVisits;
    }

    // public function sevenLastDaysVisits(): array
    // {
    //     $dailyVisits = $this->createQueryBuilder('v')
    //         ->where('v.visitDate >= :weekAgo')
    //         ->setParameter('weekAgo', new \DateTime('7 days ago'))
    //         ->groupBy('DAY(v.visitDate)')
    //         ->orderBy(  'v.visitDate', 'DESC')
    //         ->getQuery()
    //         ->getResult();

    //     $dayCounts = [];
    //     foreach ($dailyVisits as $visit) {
    //     $dayCounts[$visit->getVisitDate()->format('Y-m-d')] = count($visit);
    //     }
    //     return $dayCounts;
    // }

//     $monthlyVisits = $this->visitRepository->createQueryBuilder('v')
//     ->where('v.visitDate >= :lastMonth')
//     ->setParameter('lastMonth', new \DateTime('first day of last month'))
//     ->groupBy('MONTH(v.visitDate)')
//     ->getQuery()
//     ->getResult();

// return count($monthlyVisits);

//     $dailyVisits = $this->visitRepository->createQueryBuilder('v')
//     ->where('v.visitDate >= :weekAgo')
//     ->setParameter('weekAgo', new \DateTime('7 days ago'))
//     ->groupBy('DAY(v.visitDate)')
//     ->getQuery()
//     ->getResult();

// $dayCounts = [];
// foreach ($dailyVisits as $visit) {
//     $dayCounts[$visit->getVisitDate()->format('Y-m-d')] = count($visit);
// }
// return $dayCounts;



    public function monthlyVisits(): int
    {
        $monthlyVisits = $this->createQueryBuilder('v')
            ->where('v.visitDate >= :jan2023')
            ->setParameter('jan2023', new \DateTime('January 1 2023'))
            ->groupBy('MONTH(v.visitDate)')
            ->getQuery()
            ->getResult();

        return count($monthlyVisits);
    }


    

//     $todayVisits = $this->visitRepository->createQueryBuilder('v')
//     ->where('v.visitDate = :today')
//     ->setParameter('today', new \DateTime('today'))
//     ->getQuery()
//     ->getResult();
// $dailyVisits= $this->visitRepository->createQueryBuilder('v')
//     ->where('v.visitDate >= :jan2023')
//     ->setParameter('jan2023', new \DateTime('January 1 2023'))
//     ->groupBy('DAY(v.visitDate)')
//     ->getQuery()
//     ->getResult();
//     return count($dailyVisits);   
// dump($dailyVisits);
// $monthlyVisits = $this->visitRepository->createQueryBuilder('v')
//     ->where('v.visitDate >= :jan2023')
//     ->setParameter('jan2023', new \DateTime('January 1 2023'))
//     ->groupBy('MONTH(v.visitDate)')
//     ->getQuery()
//     ->getResult();



//    /**
//     * @return Visit[] Returns an array of Visit objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Visit
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
