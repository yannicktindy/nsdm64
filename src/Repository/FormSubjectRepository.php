<?php

namespace App\Repository;

use App\Entity\FormSubject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FormSubject>
 *
 * @method FormSubject|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormSubject|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormSubject[]    findAll()
 * @method FormSubject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormSubjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormSubject::class);
    }

//    /**
//     * @return FormSubject[] Returns an array of FormSubject objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?FormSubject
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
