<?php

namespace App\Repository;

use App\Entity\ForumRoom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ForumRoom>
 *
 * @method ForumRoom|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForumRoom|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForumRoom[]    findAll()
 * @method ForumRoom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForumRoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForumRoom::class);
    }

//    /**
//     * @return ForumRoom[] Returns an array of ForumRoom objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ForumRoom
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
