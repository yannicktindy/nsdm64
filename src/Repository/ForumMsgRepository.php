<?php

namespace App\Repository;

use App\Entity\ForumMsg;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ForumMsg>
 *
 * @method ForumMsg|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForumMsg|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForumMsg[]    findAll()
 * @method ForumMsg[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForumMsgRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForumMsg::class);
    }

//    /**
//     * @return ForumMsg[] Returns an array of ForumMsg objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ForumMsg
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
