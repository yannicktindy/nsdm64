<?php

namespace App\Service;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MailerService
{

    public function __construct(
        private MailerInterface $mailer,
        private UrlGeneratorInterface $router,
        )
    {
        $this->router = $router;
    }
    
    public function ValidationAccountEmail(
        string $to, 
        string $subject,
        string $pseudo,
        string $token,
        ): void
    {

        $email = (new TemplatedEmail())
            ->from('him@insidiome.com')
            ->to($to)
            ->subject($subject)
            ->htmlTemplate('front/email/registration.html.twig')
            ->context([
                'pseudo' => $pseudo,
                'link' => 'https://insidiome.com/ignition' . '/' . $token,
            ])
            ;
        $this->mailer->send($email);
    }

    public function registrationEmail(
        string $to, 
        string $subject,
        string $pseudo,
        string $userSlug,
        ): void
    {

        $email = (new TemplatedEmail())
            ->from('him@insidiome.com')
            ->to($to)
            ->subject($subject)
            ->htmlTemplate('front/email/registration.html.twig')
            ->context([
                'pseudo' => $pseudo,
                'userSlug' => $userSlug,
            ])
            ;
        $this->mailer->send($email);
    }

    public function RenewPassordEmail(
        string $to, 
        string $subject,
        string $token,
        ): void
    {
        $email = (new TemplatedEmail())
            ->from('him@insidiome.com')
            ->to($to)
            ->subject($subject)
            ->htmlTemplate('front/email/renewPassword.html.twig')
            ->context([
                'link' => 'https://insidiome.com/security/new-password' . '/' . $token,
            ])
            ;
        $this->mailer->send($email);
    }  
    
    public function sendRenewPasswordEmail(
        string $to, 
        string $token,
        ) 
    {

    // Générer l'URL avec le token en paramètre
    $url = $this->router->generate('app_reset_password', [
        'token' => $token
    ]);

    $email = (new TemplatedEmail())
        // ...
        ->context([
        'link' => $url,
        ])
    ;

    $this->mailer->send($email);

    }   

}    