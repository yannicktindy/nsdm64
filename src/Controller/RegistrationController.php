<?php

namespace App\Controller;

use App\Entity\Kinkster;
use App\Form\RegistrationFormType;
use App\Form\PreRegisterType;
use App\Repository\KinksterRepository;
use App\Security\KinksterAuthenticator;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\String\Slugger\SluggerInterface;


class RegistrationController extends AbstractController
{
    public function __construct( 
        private EntityManagerInterface $em,
        private KinksterRepository $kinksterRepository,

    ) {}

    #[Route('/dark-hound', name: 'app_dark_hound')]
    public function preRegister(
        Request $request, 
        UserPasswordHasherInterface $userPasswordHasher, 
        UserAuthenticatorInterface $userAuthenticator, 
        KinksterAuthenticator $authenticator, 
        EntityManagerInterface $entityManager,
        SluggerInterface $slugger,
        MailerService $mailer,
        SessionInterface $session,
    ): Response
    {
        $lang = $session->get('lang');
        $form = $this->createForm(PreRegisterType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $email = $form->get('email')->getData();
            // unicity email verification
            $user = $this->kinksterRepository->findOneBy(['email' => $email]);
            if ($user) {
                return $this->render('registration/gate.html.twig', [
                    'user' => $user,
                    'lang' => $lang,
                    'code' => 'emailexists',
                    'message' => 'Un compte existe déjà avec cette adresse email / An account already exists with this email address'
                ]);
            }


            $pseudo = $form->get('pseudo')->getData();
            $password = $form->get('plainPassword')->getData();
            // Générer 16 octets aléatoires 
            $randomBytes = random_bytes(16);
            // Ajouter la composante temps
            $timestamp = time();
            // Concaténer et encoder en base64  
            $baseString = base64_encode($randomBytes . $timestamp);
            // Remplacer certains caractères
            $baseString = str_replace(['+', '/', '='], ['-', '_', ''], $baseString);
            // Token final de 64 caractères
            $securityToken = substr($baseString, 0, 64);
            $date = new \DateTime('now');

            $user = [
                'pseudo' => $pseudo,
                'email' => $email,
                'password' => $password,
                'securityToken' => $securityToken,
                'date' => $date,
            ];
            // set user in session 
            $session->set('user', $user);
            return $this->redirectToRoute('app_gate');
        }    
        return $this->render('registration/preRegister.html.twig', [
            'PreRegisterForm' => $form->createView(),
            'lang' => $lang,
        ]);

    }

    
    #[Route('/gate', name: 'app_gate')]
    public function gate(
        SessionInterface $session,
        MailerService $mailer,
    )
    {
        $lang = $session->get('lang');
        $user = $session->get('user');

        if (!$user) {
            return $this->redirectToRoute('app_dark_hound');
        }

        $message = 'Un email vous a été envoyé, cliquez sur le liens pour activer votre compte INSIDIOME. Pensez à regarder dans vos spams / 
        An email has been sent to you, click on the link to activate your INSIDIOME account. Remember to check your spams.';
                   // do anything else you need here, like send an email
        $to = $user['email'];
        $pseudo = $user['pseudo'];
        $subject = 'Validation INSIDIOME';
        $token = $user['securityToken'];
        $mailer->ValidationAccountEmail($to, $subject, $pseudo, $token);

        return $this->render('registration/gate.html.twig', [
            'user' => $user,
            'lang' => $lang,
            'code' => 'firstValidation',
            'message' => $message,
        ]);

    }

    #[Route('/ignition/{token}', name: 'app_ignition')]
    public function ignite(
        string $token,
        SessionInterface $session,
        UserPasswordHasherInterface $userPasswordHasher, 
        EntityManagerInterface $entityManager,
    )
    {
        $lang = $session->get('lang');
        $user = $session->get('user');
        $securityToken = $user['securityToken'];

        if ($token != $securityToken) {
            return $this->redirectToRoute('app_dark_hound');
        }

        $kinkster = new Kinkster();
        $kinkster->setPseudo($user['pseudo']);
        $kinkster->setEmail($user['email']);
        $password = $user['password'];
        $kinkster->setPassword(
            $userPasswordHasher->hashPassword(
                $kinkster,
                $password
            )
        );

        $kinkster->setCreation(new \DateTime('now'));
        $kinkster->setUpdateAt(new \DateTime('now'));

        $kinkster->setRoles(['ROLE_USER']);
        $kinkster->setUserSlug('kinkster-' . uniqid());
        $kinkster->setIsValidated(true);
        $kinkster->setIsSuspended(false);
        $kinkster->setIsDeleted(false);
        if ($lang == 'fr') {
            $kinkster->setIsFr(true);

        } else {
            $kinkster->setIsFr(false);

        }
        $kinkster->setIsLetter(true);
        $kinkster->setVisit(0);

        $entityManager->persist($kinkster);
        $entityManager->flush();
        $session->remove('user');

        // hasshed password 



        $message = 'Votre compte a été activé, vous pouvez vous connecter / Your account has been activated, you can log in';

        return $this->render('registration/gate.html.twig', [
            'user' => $user,
            'lang' => $lang,
            'code' => 'secondValidation',
            'message' => $message,
        ]);



    }

    #[Route('/dungeon-gate', name: 'app_dungeon_gate')]
    public function register(
        Request $request, 
        UserPasswordHasherInterface $userPasswordHasher, 
        UserAuthenticatorInterface $userAuthenticator, 
        KinksterAuthenticator $authenticator, 
        EntityManagerInterface $entityManager,
        SluggerInterface $slugger,
        MailerService $mailer,
        SessionInterface $session,
        ): Response
    {
        $lang = $session->get('lang');
        $user = new Kinkster();
        $form = $this->createForm(RegistrationFormType::class, $user);
        // set creation 
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setCreation(new \DateTime('now'));
            $user->setUpdateAt(new \DateTime('now'));
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setRoles(['ROLE_USER']);
            $user->setUserSlug('kinkster-' . uniqid());
            $user->setIsValidated(true);
            $user->setIsSuspended(false);
            $user->setIsDeleted(false);
            if ($lang == 'fr') {
                $user->setIsFr(true);

            } else {
                $user->setIsFr(false);

            }
            $user->setIsLetter(true);
            $user->setVisit(0);

            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            // $to = $user->getEmail();
            // $subject = 'Inscription sur INSIDIOME';
            // $userSlug = $user->getUserSlug();
            // $pseudo = $user->getPseudo();
            // $mailer->registrationEmail($to, $subject, $pseudo, $userSlug);

            // return $this->redirectToRoute('app_home');

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'lang' => $lang,
        ]);
    }



    // #[Route('/register', name: 'app_register')]
    // public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserAuthenticatorInterface $userAuthenticator, KinksterAuthenticator $authenticator, EntityManagerInterface $entityManager): Response
    // {
    //     $user = new Kinkster();
    //     $form = $this->createForm(RegistrationFormType::class, $user);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         // encode the plain password
    //         $user->setPassword(
    //             $userPasswordHasher->hashPassword(
    //                 $user,
    //                 $form->get('plainPassword')->getData()
    //             )
    //         );

    //         $entityManager->persist($user);
    //         $entityManager->flush();
    //         // do anything else you need here, like send an email
            
    //         return $userAuthenticator->authenticateUser(
    //             $user,
    //             $authenticator,
    //             $request
    //         );
    //     }

    //     return $this->render('registration/register.html.twig', [
    //         'registrationForm' => $form->createView(),
    //     ]);
    // }
}
