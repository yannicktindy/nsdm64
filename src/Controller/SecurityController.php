<?php

namespace App\Controller;

use App\Form\SearchEmailType;
use App\Form\RenewPasswordType;
use App\Repository\KinksterRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityController extends AbstractController
{

    #[Route(path: '/cellardoor', name: 'app_cellar_door')]
    public function login(
        AuthenticationUtils $authenticationUtils,
        SessionInterface $session,
        ): Response
    {   

        $lang = $session->get('lang');
        if ($this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error, 'lang' => $lang]);
    }


    #[Route(path: '/sundown', name: 'app_sun_down')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route(path: '/naty-boy/{code}', name: 'app_cop')]
    public function denyBoard(
        string $code,
    ): Response
    {

        return $this->render('front/cop/copFR.html.twig', [
            'controller_name' => 'TestController',
            'code' => $code,
        ]);
    }
    
    #[Route(path: '/security/reset-password', name: 'app_reset_password')]
    public function searchEmail(
        Request $request,
        KinksterRepository $kinsterRepository,
        MailerService $mailer,
        EntityManagerInterface $entityManager,
    ): Response
    {
        $message = '';
        $form = $this->createForm(SearchEmailType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            // 1) Lookup the user by email
            $user = $kinsterRepository->findOneBy(['email' => $data['email']]);

            if (!$user) {
                // No user found, return error response
                $message = 'Email non trouvé.';
                return $this->render('security/findEmail.html.twig', [
                    'form' => $form,
                    'message' => $message,
                ]);
            } else {
                // Générer 16 octets aléatoires 
                $randomBytes = random_bytes(16);
                // Ajouter la composante temps
                $timestamp = time();
                // Concaténer et encoder en base64  
                $baseString = base64_encode($randomBytes . $timestamp);
                // Remplacer certains caractères
                $baseString = str_replace(['+', '/', '='], ['-', '_', ''], $baseString);
                // Token final de 64 caractères
                $securityToken = substr($baseString, 0, 64);
                // Enregistrer le token dans la base de données
                $user->setSecurityToken($securityToken);
                $entityManager->persist($user);
                $entityManager->flush();

                $message = 'Un email vous a été envoyé, cliquez sur le lien pour renouveler votre mot de passe. Pensez à regarder dans vos spams. / 
                 An email has been sent to you, click on the link to renew your password. Remember to check your spam.';
                            // do anything else you need here, like send an email
                $to = $form->get('email')->getData();
                $subject = 'Renouvellement mot de passe / Renew password : INSIDIOME';
                $token = $securityToken;
                $mailer->RenewPassordEmail($to, $subject, $token);
                return $this->render('security/findEmail.html.twig', [
                    'form' => $form,
                    'message' => $message,
                ]);
            }

        }

        return $this->render('security/findEmail.html.twig', [
            'form' => $form,
            'message' => $message,
        ]);
    }


    #[Route(path: '/security/new-password/{token}', name: 'app_renew_password')]
    public function renewPassword(
        string $token,
        Request $request,
        KinksterRepository $kinsterRepository,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $userPasswordHasher, 
    ): Response
    {
        
        $form = $this->createForm(RenewPasswordType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupérer l'utilisateur par le token
            $user = $kinsterRepository->findOneBy (['securityToken' => $token]);
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setSecurityToken(null);

            $entityManager->persist($user);
            $entityManager->flush();

            // Rediriger vers la page de connexion
            return $this->redirectToRoute('app_cellar_door');
        }

        return $this->render('security/resetPassword.html.twig', [
            'form' => $form,
            'message' => 'Veuillez renseigner votre nouveau mot de passe',

        ]);
    }

    
}
