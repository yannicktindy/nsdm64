<?php

namespace App\Controller\QuickAdmin;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use App\Repository\TestRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/quick')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class AdminQuickBoardController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        // private UserInterface $user,
        private KinksterRepository $kinksterRepository,
        private CommentRepository $commentRepository,
    )
    {
    }

    #[Route('/', name: 'app_quick')]
    public function quickAdmlin(
        CommentRepository $commentRepository,
        Request $request,
    ): Response
    {
        $currentPage = $request->query->getInt('page', 1);
        $perPage = 10;
        $offset = ($currentPage - 1) * $perPage;
        

        $countComment = $commentRepository->count([]);
        $totalPages = ceil($countComment / $perPage);
        // comment where isValid = false
        $comments = $commentRepository->findInvalidOrNotAnsweredComments($perPage, $offset);

        return $this->render('admin/quick/quickHome.html.twig', [
        ]);
    }

    #[Route('/comment-to-validation', name: 'app_quick_comment_to_valid')]
    public function comToValid(
        CommentRepository $commentRepository,
        Request $request,
    ): Response
    {
        $currentPage = $request->query->getInt('page', 1);
        $perPage = 10;
        $offset = ($currentPage - 1) * $perPage;
        

        $countComment = $commentRepository->count([]);
        $totalPages = ceil($countComment / $perPage);
        // comment where isValid = false
        $comments = $commentRepository->findInvalidOrNotAnsweredComments($perPage, $offset);

        return $this->render('admin/quick/commentList.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'comments' => $comments,
            'totalPages' => $totalPages,
            'currentPage' => $currentPage,
            'code' => 'Comments to validation',
        ]);
    }

    #[Route('/all-comments', name: 'app_quick_comment_list')]
    public function commentList(
        CommentRepository $commentRepository,
        Request $request,
    ): Response
    {
        $currentPage = $request->query->getInt('page', 1);
        $perPage = 10;
        $offset = ($currentPage - 1) * $perPage;
        

        $countComment = $commentRepository->count([]);
        $totalPages = ceil($countComment / $perPage);
        // comment where isValid = false
        $comments = $commentRepository->findBy(
            [], 
            ['createdAt' => 'DESC'], $perPage, $offset);

        return $this->render('admin/quick/commentList.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'comments' => $comments,
            'totalPages' => $totalPages,
            'currentPage' => $currentPage,
            'code' => 'All comments',
        ]);
    }

    #[Route('/all-users', name: 'app_quick_users_list')]
    public function users(
        KinksterRepository $kinksterRepository,
        Request $request,
    ): Response
    {
        $currentPage = $request->query->getInt('page', 1);
        $perPage = 15;
        $offset = ($currentPage - 1) * $perPage;
        

        $countKinksters = $kinksterRepository->count([]);
        $totalPages = ceil($countKinksters / $perPage);
    
        $kinksters = $kinksterRepository->findBy([], ['creation' => 'DESC'], $perPage, $offset);

        return $this->render('admin/quick/usersList.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'kinksters' => $kinksters,
            'totalPages' => $totalPages,
            'currentPage' => $currentPage,
            'code' => 'All users',
        
        ]);
    }

    #[Route('/known-users', name: 'app_quick_known_list')]
    public function known(
        KinksterRepository $kinksterRepository,
        Request $request,
    ): Response
    {
        $currentPage = $request->query->getInt('page', 1);
        $perPage = 15;
        $offset = ($currentPage - 1) * $perPage;
        

        $countKinksters = $kinksterRepository->count([]);
        $totalPages = ceil($countKinksters / $perPage);
    
        $kinksters = $kinksterRepository->findBy(
            ['isKnown' => true], 
            ['creation' => 'DESC'], 
            $perPage, $offset);
        

        return $this->render('admin/quick/usersList.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'kinksters' => $kinksters,
            'totalPages' => $totalPages,
            'currentPage' => $currentPage,
            'code' => 'Known users',
        
        ]);
    }

    #[Route('/one-user/{userSlug}', name: 'app_quick_one_user')]
    public function user(
        string $userSlug,
        KinksterRepository $kinksterRepository,
        Request $request,
    ): Response
    {
        $kinkster = $kinksterRepository->findOneBy(['userSlug' => $userSlug]);

        return $this->render('admin/quick/oneUser.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'kinkster' => $kinkster,
      
        ]);
    }

    #[Route('/mark-user-as-known/{userSlug}', name: 'app_quick_user_known')]
    public function markK(
        string $userSlug,
        KinksterRepository $kinksterRepository,
        EntityManagerInterface $em,
    ): Response
    {
        $kinkster = $kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $kinkster->setIsKnown(true);
        $em->persist($kinkster);
        $em->flush();
        return $this->redirectToRoute('app_quick_users_list', ['userSlug' => $userSlug]);

    }

    #[Route('/user-status/{userSlug}', name: 'app_quick_user_status')]
    public function status(
        string $userSlug,
        KinksterRepository $kinksterRepository,
        Request $request,
    ): Response
    {
        $kinkster = $kinksterRepository->findOneBy(['userSlug' => $userSlug]);

        return $this->render('admin/quick/oneUser.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'kinkster' => $kinkster,
      
        ]);
    }

    #[Route('/delete-user/{userSlug}', name: 'app_quick_user_delete')]
    public function adminDel(
        string $userSlug,
        KinksterRepository $kinksterRepository,
        VisitRepository $visitRepository,
        CommentRepository $commentRepository,
        ImpactRepository $impactRepository,
        ScoreRepository $scoreRepository,
        ): Response
    {
        $kinkster = $kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        if ($kinkster) {
            $visits = $kinkster->getVisits();
            if ($visits){
                foreach ($visits as $visit) {
                    $visitRepository->remove($visit, true);
                }
            }
    
            $comments = $kinkster->getComments();
            if ($comments) {
                foreach ($comments as $comment) {
                    $commentRepository->remove($comment, true);
                }
            }
        
            $impacts = $kinkster->getImpacts();
            if ($impacts) {
                foreach ($impacts as $impact) {
                    $scores = $impact->getScores();
                    if ($scores) {
                        foreach ($scores as $score) {
                            $scoreRepository->remove($score, true);
                        }
                    }
                }
            }
    
            $kinksterRepository->remove($kinkster, true);
            
        }
        return $this->redirectToRoute('app_quick_users_list');

    }

    #[Route('/tests', name: 'app_quick_tests_list')]
    public function tests(
        TestRepository $testRepository,
    )
    {
        $tests = $testRepository->findAll();
        return $this->render('admin/quick/testList.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'tests' => $tests,
      
        ]);

    }

    #[Route('/onetest/{testSlug}', name: 'app_quick_one_test')]
    public function test(
        string $testSlug,
        TestRepository $testRepository,
    )
    {
        $test = $testRepository->findOneBy(['testSlug' => $testSlug]);
        return $this->render('admin/quick/oneTest.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'test' => $test,
      
        ]);

    }

    #[Route('/todo', name: 'app_quick_todo')]
    public function todo(

    ): Response
    {
        

        return $this->render('admin/quick/todo.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            
      
        ]);
    }
}
