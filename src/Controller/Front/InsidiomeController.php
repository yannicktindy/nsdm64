<?php

namespace App\Controller\Front;

use App\Entity\Kinkster;
use App\Repository\KinksterRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class InsidiomeController extends AbstractController
{
    #[Route('/insidiome', name: 'app_insidiome')]
    public function index(
        SessionInterface $session, 
        KinksterRepository $kinksterRepository,
    ): Response
    {
        $lang = $session->get('lang');
        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }

        if ($isFrench == false) {
            return $this->render('front/insidiome/nsdmEN.html.twig', []);
        } else {
            return $this->render('front/insidiome/nsdmFR.html.twig', []);
        }

    }


}
