<?php

namespace App\Controller\Front;

use App\Entity\Visit;
use App\Form\StartType;
use App\Repository\VisitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class StartController extends AbstractController
{
    
    #[Route('/entry', name: 'app_entry')]
    public function entry(
        SessionInterface $session 
    ): Response
    {
        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {
            return $this->redirectToRoute('app_home');
        }
        
        return $this->render('front/start/enterFR.html.twig', [
            'controller_name' => 'StartController',
        ]);
    }


    // -------------------------------------------------------------------
    // old version  ------------------------------------------------------
    // -------------------------------------------------------------------
    #[Route('/', name: 'app_enter_lang')]
    public function lang(
        SessionInterface $session 
    ): Response
    {


        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {
            return $this->redirectToRoute('app_home');
        }
        
        return $this->render('front/start/enterFR.html.twig', [
            'controller_name' => 'StartController',
        ]);
    }


    #[Route('/enter/{lang}', name: 'app_enter')]
    public function gate(
        string $lang,
        SessionInterface $session 
    ): Response
    {
        // lang set in session
        $session->set('lang', $lang);

        if ($lang != 'fr' ) {
            return $this->render('front/start/gateEN.html.twig', [
                'controller_name' => 'StartController',
            ]);
        }

        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {
            return $this->redirectToRoute('app_home');
        }
        
        return $this->render('front/start/gateFR.html.twig', [
            'controller_name' => 'StartController',
        ]);
    }

    #[Route('/start/validation', name: 'app_start_validation')]
    public function valid(
        RequestStack $requestStack,
        SessionInterface $session,
        VisitRepository $visitRepository, 
    ): Response
    {

        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {

            return $this->redirectToRoute('app_home');
        }

        $lang = $session->get('lang');


        $request = $requestStack->getCurrentRequest();
        $startForm = $this->createForm(StartType::class);
        $startForm->handleRequest($request);

        if ($startForm->isSubmitted() && $startForm->isValid()) {
            $session->set('validation', true);
            $newVisit = new Visit();
            $newVisit->setVisitDate(new \DateTime('now'));
            // persit new visit
            $visitRepository->save($newVisit, true);
            $validation = $session->get('validation');

            $session->set('validation', true);

            return $this->redirectToRoute('app_home');
        }    
        if ($lang != 'fr' ) {
            return $this->render('front/start/validationEN.html.twig', [
                'controller_name' => 'StartController',
                'startForm' => $startForm->createView(),
            ]);
        }

        
        return $this->render('front/start/validationFR.html.twig', [
            'controller_name' => 'StartController',
            'startForm' => $startForm->createView(),
        ]);
    }
}    
    