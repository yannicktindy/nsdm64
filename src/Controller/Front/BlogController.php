<?php

namespace App\Controller\Front;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Utils\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class BlogController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private CommentRepository $commentRepository,
    ) {}
    #[Route('/blog', name: 'app_blog')]
    public function index(
        CategoryRepository $categoryRepository,
    ): Response
    {
                // find one category with id 1
        $category = $categoryRepository->findOneBy(['id' => 1]);
        return $this->render('front/blog/blogPlanFR.html.twig', [
            'controller_name' => 'BlogController',
            'category' => $category,
        ]);
    }

    #[Route('/blog/categorie/{categSlug}', name: 'app_blog_category')]
    public function categorie(
        string $categSlug,
        CategoryRepository $categoryRepository,
    ): Response
    {
        $categorie = $categoryRepository->findOneBy(['slug' => $categSlug]);
        $image = new Image([]);
        $randomBanner = $image->getOneRandomBanner();
        return $this->render('front/blog/oneCategorieFR.html.twig', [
            'controller_name' => 'BlogController',
            'categorie' => $categorie,
            'randomBanner' => $randomBanner,
        ]);
    }   

    #[Route('/blog/article/{articleSlug}', name: 'app_blog_article')]
    public function article(
        string $articleSlug,
        ArticleRepository $articleRepository,
        CommentRepository $commentRepository,
        CategoryRepository $categoryRepository,
        Request $request,
    ): Response
    {
        $article = $articleRepository->findOneBy(['slug' => $articleSlug]);
        $image = new Image([]);
        $randomBanner = $image->getOneRandomBanner();

        $categories = $article->getCategories();
        $comments = $commentRepository->findBy(
            ['article' => $article, 'isValid' => true],
            ['createdAt' => 'DESC'], 5); 

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && !$form->isValid()) {

            $url = $this->generateUrl('app_blog_article', [
                'articleSlug' => $articleSlug,
                '_fragment' => 'comment',
                'message' => 'Votre commentaire n\'était pas saisi correctement. Veuillez recommencer.'
            ]);

            return $this->redirect($url); 
        }
        if ($form->isSubmitted() && $form->isValid()) {
            // check i user already commented on this article
            $user = $this->getUser();
            $checkComment = $commentRepository->findOneBy(['user' => $user, 'article' => $article]);
            if ($checkComment) {
                $commentId = $checkComment->getId();
                $code = "comArticle";
                return $this->render('front/cop/copFR.html.twig', [
                    'controller_name' => 'TestController',
                    'commentId' => $commentId,
                    'code' => $code,
                ]);
            }

            $test = null;
            $comment
                ->setCreatedAt(new \DateTime())
                ->setTest($test)
                ->setUser($user)
                ->setArticle($article)
                ->setIsValid(false)
                ->setToKeep(false)
                ->setIsAnsw(false)
                ->setIsTest(false)
                ->setIsArticle(true);
            $commentRepository->save($comment, true);    

            // Générer l'URL de base 
            return $this->render('front/cop/copFR.html.twig', [
                'controller_name' => 'TestController',
                'articleSlug' => $articleSlug,
                'code' => 'succCom',
            ]);
        }    

        return $this->render('front/blog/oneArticleFR.html.twig', [
            'article' => $article,
            'comments' => $comments,
            'categories' => $categories,
            'message' => '',
            'form' => $form->createView(),
            'randomBanner' => $randomBanner,
        ]);
    }  
}
