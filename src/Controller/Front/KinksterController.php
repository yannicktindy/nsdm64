<?php

namespace App\Controller\Front;

use App\Entity\Comment;
use App\Entity\Kinkster;
use App\Form\KinksterType;
use App\Repository\CommentRepository;
use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('/kinkster')]
class KinksterController extends AbstractController
{
    public function __construct(
        private KinksterRepository $kinksterRepository,
    ) {}

    #[Route('/', name: 'app_kinkster_index', methods: ['GET'])]
    public function index(KinksterRepository $kinksterRepository): Response
    {
        return $this->render('front/kinkster/index.html.twig', [
            'kinksters' => $kinksterRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_kinkster_new', methods: ['GET', 'POST'])]
    public function new(Request $request, KinksterRepository $kinksterRepository): Response
    {
        $kinkster = new Kinkster();
        $form = $this->createForm(KinksterType::class, $kinkster);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $kinksterRepository->save($kinkster, true);

            return $this->redirectToRoute('app_profil_board', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('kinkster/new.html.twig', [
            'kinkster' => $kinkster,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_kinkster_show', methods: ['GET'])]
    public function show(Kinkster $kinkster): Response
    {
        return $this->render('kinkster/show.html.twig', [
            'kinkster' => $kinkster,
        ]);
    }

    #[Route('/{userSlug}/edit', name: 'app_kinkster_edit', methods: ['GET', 'POST'])]
    public function edit(
        string $userSlug,
        Request $request, 
        Kinkster $kinkster, 
        KinksterRepository $kinksterRepository,
        UserInterface $user,
        ): Response
    {
        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $kinksterIdentifier = $kinkster->getEmail();
        $userIdentifier = $user->getUserIdentifier();
        if ($kinksterIdentifier !== $userIdentifier) {
            return $this->render('front/profil/profil.html.twig');
        }
        $form = $this->createForm(KinksterType::class, $kinkster);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $kinksterRepository->save($kinkster, true);

            return $this->redirectToRoute('app_profil_board', ['userSlug' => $userSlug], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('front/kinkster/edit.html.twig', [
            'kinkster' => $kinkster,
            'form' => $form,
        ]);
    }




    #[Route('/{id}', name: 'app_kinkster_delete', methods: ['POST'])]
    public function delete(Request $request, Kinkster $kinkster, KinksterRepository $kinksterRepository): Response
    {

        if ($this->isCsrfTokenValid('delete'.$kinkster->getId(), $request->request->get('_token'))) {
            $kinksterRepository->remove($kinkster, true);
        }

        return $this->redirectToRoute('app_kinkster_index', [], Response::HTTP_SEE_OTHER);
    }
}
