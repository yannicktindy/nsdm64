<?php

namespace App\Controller\Front;

use App\Entity\Comment;
use App\Entity\Impact;
use App\Entity\Score;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\ImpactRepository;
use App\Repository\TestRepository;
use App\Repository\FactorRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class TestController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private TestRepository $testRepository,
        private FactorRepository $factorRepository,
        private ImpactRepository $impactRepository,
        private KinksterRepository $kinksterRepository,
        private ScoreRepository $scoreRepository,
        private CommentRepository $commentRepository,
    ) {}

    #[Route('/kink-and-bdsm-tests/list', name: 'app_test_list')]
    public function list(
        SessionInterface $session,
        KinksterRepository $kinksterRepository,
    ): Response
    {
        $lang = $session->get('lang');
        $basics = $this->testRepository->findBy(['advanced' => false]);
        $tests = $this->testRepository->findAll();

        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }

        if ($isFrench == false) {
            return $this->render('front/test/listEN.html.twig', [
                'tests' => $tests,
                'lang' => 'en',
            ]);
        } else {

            return $this->render('front/test/listFR.html.twig', [
                'tests' => $tests,
                'lang' => 'fr',
            ]);
        }
    }


    #[Route('/kink-and-bdsm-tests/present/{testSlug}', name: 'app_test')]
    public function index(
        string $testSlug,
        SessionInterface $session,
        Request $request,
        ImpactRepository $impactRepository,
        TestRepository $testRepository,
        CommentRepository $commentRepository,
        KinksterRepository $kinksterRepository,

    ): Response
    {
        $lang = $session->get('lang');
        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }
        $test = $this->testRepository->findOneBy(['testSlug' => $testSlug]);
        $factors = $test->getFactors();
        // 5 last comments
        $comments = $this->commentRepository->findBy(
            ['test' => $test, 'isValid' => true],
            ['createdAt' => 'DESC'], 5);

        $allComments = $this->commentRepository->findBy(
            ['test' => $test, 'isValid' => true],);
        $totalValue = 0;
        // Parcourir les commentaires  
        foreach ($allComments as $comment) {
            // Ajouter la valeur du commentaire au total
            $totalValue += $comment->getValue(); 
        }
        // Calculer la moyenne
        if (count($allComments) > 0) {
            $averageValue = $totalValue / count($allComments);
            $averageValue = round($averageValue, 1);
        } else {
            $averageValue = 10;   
        }      

        $message = '';
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && !$form->isValid()) {


            if ($isFrench == false) {
                // Générer l'URL avec l'ancre
                $url = $this->generateUrl('app_test', [
                    'testSlug' => $testSlug,
                    '_fragment' => 'comment',
                    'message' => 'Your comment was not entered correctly. Please try again.'
                    
                ]);
            } else {
                $url = $this->generateUrl('app_test', [
                    'testSlug' => $testSlug,
                    '_fragment' => 'comment',
                    'message' => 'Votre commentaire n\'était pas saisi correctement. Veuillez recommencer.'
                ]);
            }

            return $this->redirect($url); 
        }
    

        if ($form->isSubmitted() && $form->isValid()) {
            $test = $testRepository->findOneBy(['testSlug' => $testSlug]);
            // check i user already commented on this test
            $checkComment = $commentRepository->findOneBy(['user' => $user, 'test' => $test]);
            if ($checkComment) {
                $commentId = $checkComment->getId();
                $code = "comTest";
                return $this->render('front/cop/copFR.html.twig', [
                    'controller_name' => 'TestController',
                    'commentId' => $commentId,
                    'code' => $code,
                ]);
            }
            // $checkTest = $testRepository->findOneBy(['testSlug' => $testSlug, 'user' => $user]);
            $checkImpact = $impactRepository->findOneBy(['test' => $test, 'kinkster' => $user]);
            if (!$checkImpact) {
                $code = "comNoTest";
                return $this->render('front/cop/copFR.html.twig', [
                    'controller_name' => 'TestController',
                    'testSlug' => $testSlug,
                    'code' => $code,
                ]);
            }
            $article = null;
            $comment
                ->setCreatedAt(new \DateTime())
                ->setTest($test)
                ->setUser($user)
                ->setArticle($article)
                ->setIsValid(false)
                ->setToKeep(false)
                ->setIsAnsw(false)
                ->setIsTest(true)
                ->setIsArticle(false);
            $commentRepository->save($comment, true);    

            // Générer l'URL de base 
            return $this->render('front/cop/copFR.html.twig', [
                'controller_name' => 'TestController',
                'testSlug' => $testSlug,
                'code' => 'succCom',
            ]);

        }

        $imageUrl = 'https://www.insidiome.com/build/img/mapped/square/test/' . $test->getImage();
        if ($isFrench == false) {
            return $this->render('front/test/presentEN.html.twig', [
                'test' => $test,
                'factors' => $factors,
                'comments' => $comments,
                'form' => $form->createView(),
                'message' => $request->query->get('message'),
                'imageUrl' => $imageUrl,
                'averageValue' => $averageValue,
            ]);
        } else {    
            return $this->render('front/test/presentFR.html.twig', [
                'test' => $test,
                'factors' => $factors,
                'comments' => $comments,
                'form' => $form->createView(),
                'message' => $request->query->get('message'),
                'imageUrl' => $imageUrl,
                'averageValue' => $averageValue,
            ]);
        }
    }
    
           

    #[Route('/kink-and-bdsm-tests/running/{testSlug}', name: 'app_test_run2')]
    public function running(
        string $testSlug,
        Request $request,
        SessionInterface $session,
        EntityManagerInterface $em,
        KinksterRepository $kinksterRepository,
    ): Response
    {
        $lang = $session->get('lang');
        $test = $this->testRepository->findOneBy(['testSlug' => $testSlug]);
        $quests = $test->getQuestions();
        $countQuests = count($quests);

        // verify if user is logged in
        if($this->getUser()) {
            $userIdentifier = $this->getUser()->getUserIdentifier();
            $user = $this->kinksterRepository->findOneBy(['email' => $userIdentifier ]);
            // verify if user has already done the test
            $impact = $this->impactRepository->findOneBy(['test' => $test, 'kinkster' => $user]);
            if($impact) {
                $impactSlug = $impact->getImpactSlug();
                return $this->redirectToRoute('app_test_done', ['impactSlug' => $impactSlug]);
            }
        }
        
 
        $arrayQuests = [];
        foreach($quests as $quest) {
            $arrayQuests[] = [
                'questFr' => $quest->getQuestFr(),
                'questEn' => $quest->getQuestEng(),
                'factorId' => $quest->getFactor()->getId(),
            ];
        }
        shuffle($arrayQuests);

        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }


        if ($isFrench == false) {
            return $this->render('front/test/runningEN.html.twig', [
                'test' => $test,
                'countQuests' => $countQuests,
                'arrayQuests' => $arrayQuests,
            ]);
        } else {
            return $this->render('front/test/runningFR.html.twig', [
                'test' => $test,
                'countQuests' => $countQuests,
                'arrayQuests' => $arrayQuests,
            ]);
        }

 
    } 

    #[Route('/kink-and-bdsm-tests/treatment/{testSlug}', name: 'app_test_treatment', methods: ['POST'])]
    public function treatment(
        string $testSlug,
        Request $request,
        SessionInterface $session,
        EntityManagerInterface $em,
    ): Response
    {
        $lang = $session->get('lang');
        $test = $this->testRepository->findOneBy(['testSlug' => $testSlug]);
        $factors = $test->getFactors();
        $countQuests = count($test->getQuestions());
        $scores = [];

        if ($request->isMethod('POST')) {
            // get the data from the form
            $formData = $request->request->all();

        
            $test->setCount($test->getCount() + 1);  
            $em->persist($test);
            $em->flush();
        
            // Initialize variables
            $factors = $test->getFactors();
            // $countQuests = $test->getCountQuests();
            $choices = [];
            $factorIds = [];      
            $scores= [];


        
            // Calculate force for each factor
            foreach ($factors as $factor) {
                $force = 0;
                $factorTestId = $factor->getId();

        
                // Iterate over questions
                for ($i = 0; $i < $countQuests; $i++) {
                    $choices[$i] = $request->request->get('choice' . $i);
                    $factorDataId = $request->request->get('factorId_' . $i);
        
                    if ($factorTestId == $factorDataId) {
                        $force += $choices[$i];
                    }
                }
                $scores[] = [
                    'factor' => $factor,
                    'level' => $force,
                ];
            }            

            usort($scores, function($a, $b) {
                return $b['level'] <=> $a['level'];
            });
       
            if($this->getUser()) {
                $userIdentifier = $this->getUser()->getUserIdentifier();
                $user = $this->kinksterRepository->findOneBy(['email' => $userIdentifier ]);

                // create impact
                $userSlug= $user->getUserSlug();

                $impact = new Impact();
                $impact->setCreation(new \DateTime('now'));
                $impactSlug = 'impact-' . $test->getTestSlug() . '-' . $userSlug . '-' . uniqid();
                $impact->setImpactSlug($impactSlug);
                $impact->setTest($test = $this->testRepository->findOneBy(['testSlug' => $testSlug]));
                $impact->setKinkster($user);
                $impact->setGenLevel(0);
    
                $em->persist($impact);

                foreach($scores as $result) {
                    $score = new Score();
                    $score->setLevel($result['level']);
                    $score->setFactor($this->factorRepository->findOneBy(['id' => $result['factor']->getId()]));
                    $score->setImpact($impact);
                    $em->persist($score);
                }
    
                $em->flush();

                return $this->redirectToRoute('app_test_result', [
                    'impactSlug' => $impactSlug , 
                    'userSlug' => $userSlug]);
            
            } else {

                

                if ($lang != 'fr' || !$lang  ) {
                    return $this->render('front/test/treatEN.html.twig', [
                        'test' => $test,
                        'test' => $test,
                        'scores' => $scores,
                        
                    ]);
                }

                return $this->render('front/test/treatFR.html.twig', [
                    'controller_name' => 'TestController',
                    'test' => $test,
                    'scores' => $scores,
                    
                ]);
            }    
        }
        
    }

    #[Route('/kink-and-bdsm-tests/result/{impactSlug}', name: 'app_test_result')]
    public function result(
        string $impactSlug,
        SessionInterface $session,
        KinksterRepository $kinksterRepository,
 
    ): Response
    {   
        $lang = $session->get('lang');
        $impact = $this->impactRepository->findOneBy(['impactSlug' => $impactSlug]);
        $kinkster = $impact->getKinkster();
        $test = $impact->getTest();
        // get scores from impact order by level desc
        $scores = $this->scoreRepository->findBy(['impact' => $impact], ['level' => 'DESC']);

        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }
        $user = $this->getUser();
        $owner= 'false';
        if ($user) {
            // check if user his owner of this impact
            $email = $user->getUserIdentifier();
            $user = $kinksterRepository->findOneBy(['email' => $email]);
            $kinkster = $impact->getKinkster();
            if ($user == $kinkster) {
                $owner = 'true';
            } else {
                $owner = 'false';
            }
        }

        $imageUrl = 'https://www.insidiome.com/build/img/mapped/square/test/' . $test->getImage();
        $impactUrl = 'https://www.insidiome.com/kink-and-bdsm-tests/result/' . $impactSlug;
        if ($isFrench == false) {
            return $this->render('front/test/resultEN.html.twig', [
                'test' => $test,
                'kinkster' => $kinkster,
                'scores' => $scores,
                'impactSlug' => $impactSlug,
                'imageUrl' => $imageUrl,
                'impactUrl' => $impactUrl,
                'owner' => $owner,
            ]);
        } else {
            return $this->render('front/test/resultFR.html.twig', [
                'controller_name' => 'TestController',
                'test' => $test,
                'kinkster' => $kinkster,
                'scores' => $scores,
                'impactSlug' => $impactSlug,
                'imageUrl' => $imageUrl,
                'impactUrl' => $impactUrl,
                'owner' => $owner,
            ]);
        }

    }



    #[Route('/kink-and-bdsm-tests/done-alert/{impactSlug}', name: 'app_test_done')]
    public function anonymous(
        string $impactSlug,
    ): Response
    {
        $impact = $this->impactRepository->findOneBy(['impactSlug' => $impactSlug]);
        $code = "done";
        // $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        

        return $this->render('front/cop/copFR.html.twig', [
            'controller_name' => 'TestController',
            'impactSlug' => $impactSlug,
            'code' => $code,
        ]);
    }    


}
