<?php

namespace App\Controller\Front;

use App\Entity\Comment;
use App\Entity\Impact;
use App\Entity\Test;
use App\Form\AnswerCommentType;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\TestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/comment')]
class CommentController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        // private UserInterface $user,
        private KinksterRepository $kinksterRepository,
        private CommentRepository $commentRepository,
        private ImpactRepository $impactRepository,
    )
    {
    }

    #[Route('/add-new-comment-ajax', name: 'app_add_ajax_comment', methods: ['POST'])]
    public function ajaxComTest(

        Request $request,
        UserInterface $user,
        TestRepository $testRepository, 
        ArticleRepository $articleRepository,
        CommentRepository $commentRepository,
        KinksterRepository $kinksterRepository,
        EntityManagerInterface $em,
    ): JsonResponse
    {
        
        // ajax request to add a new comment
        if ($request->isXmlHttpRequest()) {
            dd($request->request->all());
            $testId = $request->request->get('testId');
            $userId = $request->request->get('userId');
            $value = $request->request->getInt('value');
            $message = $request->request->get('msg');
            
            $test = $testRepository->findOneBy(['id' => $testId]);
            $user = $kinksterRepository->findOneBy(['id' => $userId]);
            
            $checkComment = $commentRepository->findOneBy(['user' => $user, 'test' => $test]);
            if ($checkComment) {
                return new JsonResponse(['stop' => true]);
            }

            $comment = new Comment();
            $comment
                ->setCreatedAt(new \DateTime())
                ->setValue($value)
                ->setMsg($message)
                ->setAnsw(null)
                ->setTest($test)
                ->setArticle(null)
                ->setUser($user)
                ->setIsValid(false)
                ->setToKeep(false)
                ->setIsAnsw(false)
                ->setIsTest(true)
                ->setIsArticle(false);
            // Sauvegarder 
            $em->persist($comment);
            $em->flush();

            return new JsonResponse([
                'success' => true,
                
            ]);
        }

        return new JsonResponse([
            'error' => 'Invalid request'
        ]);
    }
    
    #[Route('/add-new-comment/{slug}/{type}', name: 'app_add_comment', methods: ['GET', 'POST'])]
    public function newComTest(
        string $type,
        string $slug,   
        Request $request,
        UserInterface $user,
        TestRepository $testRepository, 
        ArticleRepository $articleRepository,
        CommentRepository $commentRepository,
        KinksterRepository $kinksterRepository,
        EntityManagerInterface $em,
    ): Response
    {
        $userIdentifier = $this->getUser()->getUserIdentifier();
        $kinkster = $this->kinksterRepository->findOneBy(['email' => $userIdentifier]);
        $test= null;
        $article = null;
        if ($type === 'test') {
            $test = $testRepository->findOneBy(['testSlug' => $slug]);
            // check i user already commented on this test
            $checkComment = $commentRepository->findOneBy(['user' => $user, 'test' => $test]);
            if ($checkComment) {
                $code = "comTest";
                return $this->render('front/cop/copFR.html.twig', [
                    'controller_name' => 'TestController',
                    'code' => $code,
                ]);
            }
            // check if user have done the test, join impact
            $impact = $this->impactRepository->findOneBy(['kinkster' => $kinkster, 'test' => $test]);
            // $impact = $this->em->createQueryBuilder()
            //     ->select('i')
            //     ->from(Impact::class, 'i') 
            //     ->where('i.kinkster = :kinkster')
            //     ->andWhere('i.test = :test')
            //     ->setParameter('kinkster', $kinkster)
            //     ->setParameter('test', $test)
            //     ->getQuery()
            //     ->getOneOrNullResult();
                

            if (!$impact) {
                $code = "comNoTest";
                return $this->render('front/cop/copFR.html.twig', [
                    'controller_name' => 'TestController',
                    'code' => $code,
                ]);
            }
        } elseif ($type === 'article') {
            $article = $articleRepository->findOneBy(['articleSlug' => $slug]);
        }

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
            $comment
                ->setCreatedAt(new \DateTime())
                ->setTest($test)
                ->setArticle($article)
                ->setUser($user)
                ->setIsValid(false)
                ->setToKeep(false)
                ->setIsAnsw(false);
                if ($type === 'test') {
                    $comment
                        ->setIsTest(true)
                        ->setIsArticle(false);
                } elseif ($type ==='article') {
                    $comment
                        ->setIsTest(false)
                        ->setIsArticle(true);
                }

                
            ;
            $commentRepository->save($comment, true);

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('front/comment/newComFR.html.twig', [
            'comment' => $comment,
            'form' => $form,
            'test' => $test,
            'code' => 'test'
        ]);
    }

    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/add-comment/answer/{comId}', name: 'app_new_answer', methods: ['GET', 'POST'])]
    public function newComTestAnsw(
        Int $comId,
        Request $request,
        UserInterface $user,
        TestRepository $testRepository, 
        CommentRepository $commentRepository,
        ImpactRepository $impactRepository,
    ): Response
    {
        $comment = null; 
        $comment = $commentRepository->findOneBy(['id' => $comId]);

        $form = $this->createForm(AnswerCommentType::class, $comment);
        if ($comment->getAnsw()) {
            $form->get('answ')->setData($comment->getAnsw());
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $answ = $data->getAnsw();
            $toKeep = $data->isToKeep();
            $comment
                ->setAnsw($answ)
                ->setToKeep($toKeep)
                ->setIsAnsw(true)
            ;
            $commentRepository->save($comment, true);

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('front/comment/answCom.html.twig', [
            'comment' => $comment,
            'form' => $form,

        ]);
    }


    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/valid-comment/{comId}', name: 'app_comment_validation', methods: ['GET', 'POST'])]
    public function validComment(
        Int $comId,
        Request $request,
        UserInterface $user,
        TestRepository $testRepository, 
        CommentRepository $commentRepository,
        EntityManagerInterface $em,
    ): Response
    {

        $comment = $commentRepository->findOneBy(['id' => $comId]);
        $comment->setIsValid(true);
        $em->persist($comment);
        $em->flush();

        return $this->redirectToRoute('app_quick_comment_to_valid', [], Response::HTTP_SEE_OTHER);

    }

    #[Route('/show-one-comment/{commentId}/{type}', name: 'app_show_one_comment')]
    public function showCom(
        CommentRepository $commentRepository
    ): Response
    {
        return $this->render('front/comment/oneConUserFR.html.twig', [
            'comments' => $commentRepository->findAll(),
        ]);
    }


    #[Route('/', name: 'app_comment_index', methods: ['GET'])]
    public function index(CommentRepository $commentRepository): Response
    {
        return $this->render('comment/index.html.twig', [
            'comments' => $commentRepository->findAll(),
        ]);
    }

    // #[Route('/new', name: 'app_comment_new', methods: ['GET', 'POST'])]
    // public function new(Request $request, CommentRepository $commentRepository): Response
    // {
    //     $comment = new Comment();
    //     $form = $this->createForm(CommentType::class, $comment);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $commentRepository->save($comment, true);

    //         return $this->redirectToRoute('app_comment_index', [], Response::HTTP_SEE_OTHER);
    //     }

    //     return $this->renderForm('comment/new.html.twig', [
    //         'comment' => $comment,
    //         'form' => $form,
    //     ]);
    // }

    #[Route('/{id}', name: 'app_comment_show', methods: ['GET'])]
    public function show(Comment $comment): Response
    {
        return $this->render('comment/show.html.twig', [
            'comment' => $comment,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_comment_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Comment $comment, CommentRepository $commentRepository): Response
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentRepository->save($comment, true);

            return $this->redirectToRoute('app_comment_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('comment/edit.html.twig', [
            'comment' => $comment,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/erase-this/{comId}', name: 'app_comment_admin_delete')]
    public function adminDel(
        Int $comId,
        CommentRepository $commentRepository,
        ): Response
    {

        $comment = $commentRepository->find($comId);
        $commentRepository->remove($comment, true);

        return $this->redirectToRoute('app_quick_comment_to_valid', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/owner-erase/{comId}', name: 'app_comment_owner_delete', methods: ['POST'])]
    public function ownerDel(
        Int $comId,
        Request $request, 
        CommentRepository $commentRepository
        ): Response
    {
        $kinkster = $this->getUser();
        $comment = $commentRepository->findOneBy(['id' => $comId]);
        // dd($comment);
        $user = $comment->getUser();
        $userSlug = $user->getUserSlug();
        if (!$kinkster === $user) {
            return $this->render('front/cop/copFR.html.twig', [
                'code' => 403,
            ]);    
        }
        
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $commentRepository->remove($comment, true);
        }

        return $this->redirectToRoute('app_profil_comments', ['userSlug' => $userSlug], Response::HTTP_SEE_OTHER);
    }
}
