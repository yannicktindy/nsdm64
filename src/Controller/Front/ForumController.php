<?php

namespace App\Controller\Front;

use App\Entity\ForumMsg;
use App\Entity\ForumSection;
use App\Repository\ForumSectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Image;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Form\Forum\AddSectionType;
use App\Repository\ForumMsgRepository;
use App\Repository\ForumSubjectRepository;
use App\Form\MessageType;

class ForumController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private ForumSectionRepository $testRepository,

    ) {}

    #[Route('/forum', name: 'app_forum')]
    public function index(
        SessionInterface $session,
        Request $request,
        ForumSectionRepository $testRepository,
    ): Response
    {

        $formSection = $this->createForm(AddSectionType::class);
        $image = new Image([]);
        $randomBanner = $image->getOneRandomBanner();
        $fsctns = $testRepository->findAll();
        return $this->render('front/forum/forum.html.twig', [
            'randomBanner' => $randomBanner,
            'fsctns' => $fsctns,
            'formSection' => $formSection->createView(),
            
        ]);
    }

    #[Route('/forum/subject/{slugFr}', name: 'app_forum_subject')]
    public function forumSubject(
        string $slugFr,
        SessionInterface $session,
        Request $request,
        ForumSubjectRepository $testRepository,
        ForumMsgRepository $messageRepository,
    ): Response
    {
        $image = new Image([]);
        $randomBanner = $image->getOneRandomBanner();
        $subject = $testRepository->findOneBy(['slugFr' => $slugFr]);

        $currentPage = $request->query->getInt('page', 1);
        $perPage = 20;
        $offset = ($currentPage - 1) * $perPage;

        $countMessage = $messageRepository->count([]);
        $totalPages = ceil($countMessage / $perPage);
        $messages = $subject->getMessages();
        return $this->render('front/forum/subject.html.twig', [
            'randomBanner' => $randomBanner,
            'subject' => $subject,
            'messages' => $messages,
            
        ]);
    }

    #[Route('/forum/add-message-on/{slugFr}', name: 'app_forum_subject_add_message',  methods: ['GET', 'POST'])]
    public function addSubject(
        string $slugFr,
        SessionInterface $session,
        Request $request,
        ForumSubjectRepository $ForumSubjectRepository,
    ): Response
    {
        $image = new Image([]);
    $randomBanner = $image->getOneRandomBanner();
    $subject = $ForumSubjectRepository->findOneBy(['slugFr' => $slugFr]);
    $messages = $subject->getMessages();

    $message = new ForumMsg();
    $form = $this->createForm(MessageType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $formData = $form->getData();
        $content = $_POST["message"]["content"];
        $message->setContenu($content);
        $message->setSubject($subject);
        $message->setKinkster($this->getUser());
        $message->setCreatedAt(new \DateTimeImmutable());
        $message->setUpdatedAt(new \DateTime());
        $message->setIsPin(false);

        $subject->setUpdatedAt(new \DateTime());

        $this->em->persist($message);
        $this->em->persist($subject);
        $this->em->flush();

        return $this->redirectToRoute('app_forum_subject', ['slugFr' => $slugFr]);
    }

        return $this->render('front/forum/addMsg.html.twig', [
            'randomBanner' => $randomBanner,
            'subject' => $subject,
            'messages' => $messages,
            'form' => $form->createView(),
        ]);
    }
  
}
