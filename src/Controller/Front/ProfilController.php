<?php

namespace App\Controller\Front;

use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use App\Form\DescriptionType;
use App\Repository\CommentRepository;
use App\Repository\PositionRepository;
use App\Repository\FetishRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
// use Symfony\Component\Security\Core\Exception\AccessDeniedException;
// use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class ProfilController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private KinksterRepository $kinksterRepository,
        private ScoreRepository $scoreRepository,
        private ImpactRepository $impactRepository,
        private PositionRepository $positionRepository,
        private FetishRepository $fetishRepository,
        private CommentRepository $commentRepository,
        // private AuthorizationCheckerInterface $authorizationChecker,
    ) {
        // $this->authorizationChecker = $authorizationChecker;
    }

    #[Route('/profil/{userSlug}', name: 'app_profil')]
    public function index(
        string $userSlug,
        SessionInterface $session,
        UserInterface $user,
        KinksterRepository $kinksterRepository,
    ): Response
    {
        $lang = $session->get('lang');
        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $impacts = $this->impactRepository->findBy(['kinkster' => $kinkster]);

        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }

        
        if ($isFrench == false) {
            return $this->render('front/profil/profilEN.html.twig', [
                'controller_name' => 'ProfilController',
                'kinkster' => $kinkster,
                'impacts' => $impacts,
                // 'nbAsnw' => $nbAnsw,
            ]);
        } else {
            return $this->render('front/profil/profilFR.html.twig', [
                'controller_name' => 'ProfilController',
                'kinkster' => $kinkster,
                'impacts' => $impacts,
                // 'nbAsnw' => $nbAnsw,
            ]);    
        }
        
        // return $this->render('front/profil/profilFR.html.twig', [
        //     'controller_name' => 'ProfilController',
        //     'kinkster' => $kinkster,
        //     'impacts' => $impacts,
        //     // 'avatar' => $avatar,
        // ]);

    }

    #[Route('/profil/activate/{userSlug}', name: 'app_profil_activate')]
    public function activate(
        string $userSlug,
        EntityManagerInterface $entityManager,
        ): Response
    {
        $user = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $user->setIsValidated(true);
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->render('/front/profil/activate.html.twig', [
            'user' => $user,
        ]);
    }



    #[Route('/profil/board/{userSlug}', name: 'app_profil_board')]
    public function board(
        string $userSlug,
        UserInterface $user,
        SessionInterface $session,
        KinksterRepository $kinksterRepository,
    ): Response
    {
        $lang = $session->get('lang');

        if (!$this->ownerCheck($userSlug, $user)) {
            return $this->render('front/cop/copFR.html.twig', [
                'code' => 403,
            ]);    
        }

        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        // $nbAnsw = $this->commentRepository->countAnswersByUser($kinkster);
        $impacts = $this->impactRepository->findBy(['kinkster' => $kinkster]);
        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }

        if ($isFrench == false) {
            return $this->render('front/profil/boardEN.html.twig', [
                'controller_name' => 'ProfilController',
                'kinkster' => $kinkster,
                'impacts' => $impacts,
                // 'nbAsnw' => $nbAnsw,
            ]);
        } else {
            return $this->render('front/profil/boardFR.html.twig', [
                'controller_name' => 'ProfilController',
                'kinkster' => $kinkster,
                'impacts' => $impacts,
                // 'nbAsnw' => $nbAnsw,
            ]);    
        }
    }

    #[Route('/profil/board/description/{userSlug}', name: 'app_profil_description')]
    public function descritpion(
        string $userSlug,
        UserInterface $user,
        Request $request, 
    ): Response
    {
        if (!$this->ownerCheck($userSlug, $user)) {
            return $this->render('front/cop/copFR.html.twig', [
                'code' => 403,
            ]);    
        }

        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);

       
        $form = $this->createForm(DescriptionType::class, $kinkster);
        if ($kinkster->getDescription()) {
            $form->get('description')->setData($kinkster->getDescription());
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $description = $form->get('description')->getData();
            $kinkster->setDescription($description);
            $this->em->persist($kinkster);
            $this->em->flush();
            return $this->redirectToRoute('app_profil_board', [
                'userSlug' => $userSlug,
            ]);
        }

        return $this->render('front/profil/FormContainer.html.twig', [
            'controller_name' => 'ProfilController',
            'form' => $form->createView(),
            'kinkster' => $kinkster,
            'code' => 'description',
        ]);    
    }

    #[Route('/profil/board/show-position/{userSlug}', name: 'app_profil_show_position')]
    public function position(
        string $userSlug,
        UserInterface $user,
    ): Response
    {
        if (!$this->ownerCheck($userSlug, $user)) {
            return $this->render('front/cop/copFR.html.twig', [
                'code' => 403,
            ]);    
        }

        $positions = $this->positionRepository->findAll();

        return $this->render('front/profil/FormContainer.html.twig', [
            'controller_name' => 'ProfilController',
            'positions' => $positions,
            'code' => 'position',
            'userSlug' => $userSlug,
        ]);    
    }

    #[Route('/profil/board/add-position/{userSlug}/{positionId}', name: 'app_profil_add_position')]
    public function addPosition(
        string $userSlug,
        int $positionId,
    ): Response
    {
        $kinster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $position = $this->positionRepository->findOneBy(['id' => $positionId]);

        $kinster->setPosition($position);
        $this->em->persist($kinster);
        $this->em->flush();
        
        return $this->redirectToRoute('app_profil_board', [
            'userSlug' => $userSlug,
        ]);
  
    }

    #[Route('/profil/board/show-fetish/{userSlug}', name: 'app_profil_show_fetish')]
    public function fetish(
        string $userSlug,
        UserInterface $user,
    ): Response
    {
        if (!$this->ownerCheck($userSlug, $user)) {
            return $this->render('front/cop/copFR.html.twig', [
                'code' => 403,
            ]);    
        }

        $fetishes = $this->fetishRepository->findAll();

        return $this->render('front/profil/FormContainer.html.twig', [
            'controller_name' => 'ProfilController',
            'fetishes' => $fetishes,
            'code' => 'fetish',
            'userSlug' => $userSlug,
        ]);    
    }

    #[Route('/profil/board/add-fetish/{userSlug}/{fetishId}', name: 'app_profil_add_fetish')]
    public function addFetish(
        string $userSlug,
        int $fetishId,
    ): Response
    {
        $kinster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $fetish = $this->fetishRepository->findOneBy(['id' => $fetishId]);

        $kinster->setFetish($fetish);
        $this->em->persist($kinster);
        $this->em->flush();
        
        return $this->redirectToRoute('app_profil_board', [
            'userSlug' => $userSlug,
        ]);
  
    }

    #[Route('/profil/board/comments/{userSlug}', name: 'app_profil_comments')]
    public function userComments(
        string $userSlug,
        UserInterface $user,
        Request $request,
        CommentRepository $commentRepository,
    ): Response
    {
        if (!$this->ownerCheck($userSlug, $user)) {
            return $this->render('front/cop/copFR.html.twig', [
                'code' => 403,
            ]);    
        }

        $currentPage = $request->query->getInt('page', 1);
        $perPage = 10;
        $offset = ($currentPage - 1) * $perPage;
        

        $countComment = $commentRepository->count([]);
        $totalPages = ceil($countComment / $perPage);
        // comment where isValid = 
        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $comments = $this->commentRepository->findBy(
            ['user' => $kinkster, 'isValid' => true],
            ['createdAt' => 'DESC'], $perPage, $offset);
        // $nbAnsw = $this->commentRepository->countAnswersByUser($kinkster);
        
        return $this->render('front/profil/userCommentsFR.html.twig', [
            'controller_name' => 'ProfilController',
            'comments' => $comments,
            'totalPages' => $totalPages,
            'currentPage' => $currentPage,
            'userSlug' => $userSlug,
            // 'nbAsnw' => $nbAnsw,
        ]);    
  
    }

    #[Route('/profil/board/delete-test/{userSlug}/{impactId}', name: 'app_profil_del_impact')]
    public function delImpact(
        string $userSlug,
        int $impactId,
    ): response
    {
        $impact = $this->impactRepository->findOneBy(['id' => $impactId]);
        $scores = $this->scoreRepository->findBy(['impact' => $impact]);
        foreach ($scores as $score) {
            $this->em->remove($score);
        }
        $this->em->remove($impact);
        $this->em->flush();

        return $this->redirectToRoute('app_profil_board', [
            'userSlug' => $userSlug,
        ]);

    }

    public function ownerCheck(
        string $userSlug,
        UserInterface $user,
    ): bool
    {
        $userIdentifier = $user->getUserIdentifier();
        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $kinksterIdentifier = $kinkster->getEmail();
        if ($kinksterIdentifier !== $userIdentifier) {
            return false;
        }
        return true;
    }


}
