<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SkynnController extends AbstractController
{

    public function __construct(

    ) {}

    #[Route('/skynn', name: 'app_skynn')]
    public function home(
        
    ): Response
    {  
                

        return $this->render('front/skynn/demo.html.twig', [
            
        ]);

    }
}