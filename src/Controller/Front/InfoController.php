<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InfoController extends AbstractController
{

    #[Route('/info/{code}', name: 'app_info')]
    public function legals(
        string $code
    ): Response
    {
        return $this->render('front/info/infoFR.html.twig', [
            'code' => $code,
        ]);
    }


    // #[Route('/info/legals', name: 'app_info_legals')]
    // public function legals(): Response
    // {
    //     return $this->render('front/info/legals.html.twig', [
    //         'controller_name' => 'InfoController',
    //     ]);
    // }

    // #[Route('/info/cgu', name: 'app_info_cgu')]
    // public function cgu(): Response
    // {
    //     return $this->render('front/info/cgu.html.twig', [
    //         'controller_name' => 'InfoController',
    //     ]);
    // }

    
}
