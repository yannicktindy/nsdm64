<?php

namespace App\Controller\Front;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\AvatarRepository;
use App\Repository\KinksterRepository;
use App\Repository\TestRepository;
use App\Repository\VisitRepository;
use App\Utils\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class HomeController extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em,
        private TestRepository $testRepository
    ) {}

    #[Route('/home', name: 'app_home')]
    public function home(
        KinksterRepository $kinksterRepository,
        SessionInterface $session,
    ): Response
    {  
                
        $lang = $session->get('lang');
        $image = new Image([]);
        $randomBanner = $image->getOneRandomBanner();
        $basicTests = $this->testRepository->findBy(['advanced' => false]);

        // find  5 last kinksters
        $kinksters = $kinksterRepository->findBy([], ['creation' => 'DESC'], 5, 0);

        $isFrench = false;
        if ($lang == 'fr' ) {
            $isFrench = true;
        } 
        $user = $this->getUser();
        if ($user) {
            $email = $user->getUserIdentifier();
            $user = $kinkster = $kinksterRepository->findOneBy(['email' => $email]);
            $userLang = $user->isIsFr();
            if ($userLang == true) {
                $isFrench = true;
            } else {
                $isFrench = false;
            }
            
        }
        if ($isFrench == false) {
            return $this->render('front/home/homeEN.html.twig', [
                'controller_name' => 'StartController',
                'tests' => $basicTests,
                'kinksters' => $kinksters,
                'lang' => 'en',
                'randomBanner' => $randomBanner,
            ]);
        } else { return $this->render('front/home/homeFR.html.twig', [
            'controller_name' => 'HomeController',
            'tests' => $basicTests,
            'kinksters' => $kinksters,
            'lang' => 'fr',
            'randomBanner' => $randomBanner,
            ]);

        }


    }
}
