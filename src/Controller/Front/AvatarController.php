<?php

namespace App\Controller\Front;

use App\Entity\Avatar;
use App\Form\AvatarType;
use App\Repository\AvatarFamillyRepository;
use App\Repository\AvatarRepository;
use App\Repository\KinksterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Knp\Component\Pager\PaginatorInterface;

#[Route('/avatar')]
class AvatarController extends AbstractController
{
    #[Route('/', name: 'app_avatar_index')]
    public function index(
        AvatarRepository $avatarRepository,
        Request $request,
        PaginatorInterface $paginator,
        ): Response
    {
        // order by createdAt desc
        $avatarsQuery = $avatarRepository->findBy([], ['createdAt' => 'DESC']);

        $avatars = $paginator->paginate(
            $avatarsQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            16 /*limit per page*/
        );

        return $this->render('front/avatar/index.html.twig', [
            'avatars' => $avatars,

        ]);
    }

    #[Route('/choice/{avatarId}/{userSlug}', name: 'app_avatar_choice')]
    public function choice(
        int $avatarId,
        string $userSlug,
        AvatarRepository $avatarRepository,
        KinksterRepository  $kinksterRepository,
        UserInterface $user,
        ): Response
    {
        $user = $kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $avatar = $avatarRepository->find($avatarId);
        $avatar->addUser($user);
        $avatarRepository->save($avatar, true);

        return $this->redirectToRoute('app_profil_board', ['userSlug' => $userSlug]);
    }



    #[Route('/new', name: 'app_avatar_new', methods: ['GET', 'POST'])]
    public function new(Request $request, AvatarRepository $avatarRepository): Response
    {
        $avatar = new Avatar();
        $form = $this->createForm(AvatarType::class, $avatar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $avatarRepository->save($avatar, true);

            return $this->redirectToRoute('app_avatar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('avatar/new.html.twig', [
            'avatar' => $avatar,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_avatar_show', methods: ['GET'])]
    public function show(Avatar $avatar): Response
    {
        return $this->render('avatar/show.html.twig', [
            'avatar' => $avatar,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_avatar_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Avatar $avatar, AvatarRepository $avatarRepository): Response
    {
        $form = $this->createForm(AvatarType::class, $avatar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $avatarRepository->save($avatar, true);

            return $this->redirectToRoute('app_avatar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('avatar/edit.html.twig', [
            'avatar' => $avatar,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_avatar_delete', methods: ['POST'])]
    public function delete(Request $request, Avatar $avatar, AvatarRepository $avatarRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$avatar->getId(), $request->request->get('_token'))) {
            $avatarRepository->remove($avatar, true);
        }

        return $this->redirectToRoute('app_avatar_index', [], Response::HTTP_SEE_OTHER);
    }

    
}
