<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Utils\Image;

class LaboController extends AbstractController
{
    #[Route('/labo', name: 'app_labo')]
    public function index(): Response
    {
        $image = new Image([]);
        $randomBanner = $image->getOneRandomBanner();
        return $this->render('front/labo/index.html.twig', [
            'controller_name' => 'LaboController',
            'randomBanner' => $randomBanner
        ]);
    }
}
