<?php

namespace App\Controller\Back;

use App\Entity\Comment;
use App\Entity\Test;
use App\Form\Admin\TestType;
use App\Repository\CommentRepository;
use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use App\Repository\TestRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/back/nsdm/test')]
// #[IsGranted('ROLE_SUPER_ADMIN')]
class ATestController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        // private UserInterface $user,
        private KinksterRepository $kinksterRepository,
        private CommentRepository $commentRepository,
    )
    {
    }

    #[Route('/', name: 'app_back_test')]
    public function back(
        TestRepository $testRepository,
        Request $request,
    ): Response
    {   
        $tests = $testRepository->findAll();
        return $this->render('adminBoard/A-test/list.html.twig', [
            'tests' => $tests,
        ]);
    }

    #[Route('/new', name: 'app_back_test_new')]
    public function newTest(
        Request $request, 
        EntityManagerInterface $entityManager
    ): Response
    {
        $test = new Test();
        $form = $this->createForm(TestType::class, $test);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($test);
            $entityManager->flush();
            // Récupérer l'ID du test nouvellement créé
            $testId = $test->getId();

            // Passez l'ID comme paramètre dans redirectToRoute
            return $this->redirectToRoute('app_back_test_full_edit', ['id' => $testId], Response::HTTP_SEE_OTHER);
        }

        return $this->render('adminBoard/A-test/new.html.twig', [
            'test' => $test,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_back_test_show', methods: ['GET'])]
    public function show(
        int $id,
        ): Response
    {
        $test = $this->em->getRepository(Test::class)->find($id);
        return $this->render('adminBoard/A-test/show.html.twig', [
            'test' => $test,
        ]);
    }

    #[Route('/{id}/full-edit', name: 'app_back_test_full_edit', methods: ['GET', 'POST'])]
    public function fullEdit(
        int $id,
        Request $request, Test $test, 
        EntityManagerInterface $entityManager
        ): Response
    {
        $form = $this->createForm(TestType::class, $test);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_back_test_full_edit', ['id' => $id], Response::HTTP_SEE_OTHER);
        }

        return $this->render('adminBoard/A-test/fullEdit.html.twig', [
            'test' => $test,
            'form' => $form,
        ]);
    }


    #[Route('/{id}/edit', name: 'app_back_test_edit', methods: ['GET', 'POST'])]
    public function edit(
        int $id,
        Request $request, 
        Test $test, 
        EntityManagerInterface $entityManager
        ): Response
    {
        $form = $this->createForm(TestType::class, $test);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_back_test_full_edit', ['id' => $id], Response::HTTP_SEE_OTHER);
        }

        return $this->render('adminBoard/A-test/edit.html.twig', [
            'test' => $test,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_back_test_delete', methods: ['POST'])]
    public function delete(Request $request, Test $test, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$test->getId(), $request->request->get('_token'))) {
            $entityManager->remove($test);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_back_test', [], Response::HTTP_SEE_OTHER);
    }
    
}