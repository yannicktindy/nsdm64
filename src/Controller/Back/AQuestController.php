<?php

namespace App\Controller\Back;

use App\Entity\Comment;
use App\Entity\Factor;
use App\Entity\Question;
use App\Entity\Test;
use App\Form\Admin\QuestionType as AdminQuestionType;
use App\Form\Admin\TestType;
use App\Form\FactorType;
use App\Form\QuestionType;
use App\Repository\CommentRepository;
use App\Repository\FactorRepository;
use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\QuestionRepository;
use App\Repository\ScoreRepository;
use App\Repository\TestRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/back/nsdm/question')]
// #[IsGranted('ROLE_SUPER_ADMIN')]
class AQuestController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        // private UserInterface $user,
        private KinksterRepository $kinksterRepository,
        private CommentRepository $commentRepository,
    )
    {
    }


    #[Route('/new-question/on-factor/{id}', name: 'app_back_question_new', methods: ['GET', 'POST'])]
    public function new(
        int $id,
        Request $request, 
        EntityManagerInterface $entityManager
        ): Response
    {
        $factor = $this->em->getRepository(Factor::class)->find($id);
        $test= $factor->getTest();
        $testID = $factor->getTest()->getId();
        $question = new Question();
        $form = $this->createForm(AdminQuestionType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $question->setFactor($factor);
            $entityManager->persist($question);
            $entityManager->flush();

            return $this->redirectToRoute('app_back_test_full_edit', ['id' => $testID], Response::HTTP_SEE_OTHER);
        }

        return $this->render('adminBoard/A-test/testQuest/new.html.twig', [
            'question' => $question,
            'factor' => $factor,
            'test' => $test,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_back_question_edit', methods: ['GET', 'POST'])]
    public function edit(
        int $id,
        Request $request, EntityManagerInterface $entityManager): Response
    {
        // $factor = $this->em->getRepository(Factor::class)->find($id);
        $question = $this->em->getRepository(Question::class)->find($id);
        $factor = $question->getFactor();
        // $test= $factor->getTest();
        $testID = $factor->getTest()->getId();
        $form = $this->createForm(AdminQuestionType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_back_test_full_edit', ['id' => $testID], Response::HTTP_SEE_OTHER);
        }

        return $this->render('adminBoard/A-test/testQuest/edit.html.twig', [
            'question' => $question,
            // 'factor' => $factor,
            // 'test' => $test,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_back_question_delete', methods: ['POST'])]
    public function delete(Request $request, Question $question, EntityManagerInterface $entityManager): Response
    {
        $factor = $question->getFactor();
        $testID = $factor->getTest()->getId();
        if ($this->isCsrfTokenValid('delete'.$question->getId(), $request->request->get('_token'))) {
            $entityManager->remove($question);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_back_test_full_edit', ['id' => $testID], Response::HTTP_SEE_OTHER);
    }

    // #[Route('/', name: 'app_question_index', methods: ['GET'])]
    // public function index(QuestionRepository $questionRepository): Response
    // {
    //     return $this->render('question/index.html.twig', [
    //         'questions' => $questionRepository->findAll(),
    //     ]);
    // }

    // #[Route('/{id}', name: 'app_question_show', methods: ['GET'])]
    // public function show(Question $question): Response
    // {
    //     return $this->render('question/show.html.twig', [
    //         'question' => $question,
    //     ]);
    // }
}
