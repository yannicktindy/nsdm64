<?php

namespace App\Controller\Back;

use App\Entity\Comment;
use App\Entity\Factor;
use App\Entity\Test;
use App\Form\Admin\FactorType;
use App\Form\Admin\TestType;

use App\Repository\CommentRepository;
use App\Repository\FactorRepository;
use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use App\Repository\TestRepository;
use App\Repository\VisitRepository;
use Container2YNkrLb\getFactorTypeService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/back/nsdm/factor')]
// #[IsGranted('ROLE_SUPER_ADMIN')]
class AFactorController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        // private UserInterface $user,
        private KinksterRepository $kinksterRepository,
        private CommentRepository $commentRepository,
    )
    {
    }



    #[Route('/new-factor/on-Test/{id}', name: 'app_back_factor_new', methods: ['GET', 'POST'])]
    public function new(
        int $id,
        Request $request, 
        TestRepository $testRepository,
        EntityManagerInterface $entityManager
        ): Response
    {
        $test = $testRepository->find($id); 
        $factor = new Factor();
        $form = $this->createForm(FactorType::class, $factor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $factor->setTest($test);
            $entityManager->persist($factor);
            $entityManager->flush();

            return $this->redirectToRoute('app_back_test_full_edit', ['id' => $id], Response::HTTP_SEE_OTHER);
        }

        return $this->render('adminBoard/A-test/testFactor/new.html.twig', [
            'factor' => $factor,
            'test' => $test,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_back_factor_edit', methods: ['GET', 'POST'])]
    public function edit(
    Request $request, Factor $factor, EntityManagerInterface $entityManager): Response
    {
        $testId = $factor->getTest()->getId();
        $form = $this->createForm(FactorType::class, $factor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_back_test_full_edit', ['id' => $testId], Response::HTTP_SEE_OTHER);
        }

        return $this->render('adminBoard/A-test/testFactor/edit.html.twig', [
            'factor' => $factor,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_back_factor_delete', methods: ['POST'])]
    public function delete(Request $request, Factor $factor, EntityManagerInterface $entityManager): Response
    {
        $testID = $factor->getTest()->getId();
        if ($this->isCsrfTokenValid('delete'.$factor->getId(), $request->request->get('_token'))) {
            $entityManager->remove($factor);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_back_test_full_edit', ['id' => $testID], Response::HTTP_SEE_OTHER);
    }

    //     #[Route('/', name: 'app_back_factor')]
//     public function back(

//         Request $request,
//     ): Response
//     {   

//         return $this->render('adminBoard/A-test/test.html.twig', [

//    ]);
//     }

    // #[Route('/', name: 'app_back_factor_index', methods: ['GET'])]
    // public function index(FactorRepository $factorRepository): Response
    // {
    //     return $this->render('factor/index.html.twig', [
    //         'factors' => $factorRepository->findAll(),
    //     ]);
    // }

    // #[Route('/{id}', name: 'app_back_factor_show', methods: ['GET'])]
    // public function show(Factor $factor): Response
    // {
    //     return $this->render('factor/show.html.twig', [
    //         'factor' => $factor,
    //     ]);
    // }
}