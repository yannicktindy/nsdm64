<?php

namespace App\Controller\Back;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use App\Repository\TestRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/back/nsdm')]
// #[IsGranted('ROLE_SUPER_ADMIN')]
class NSDMAdminController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        // private UserInterface $user,
        private KinksterRepository $kinksterRepository,
        private CommentRepository $commentRepository,
    )
    {
    }

    #[Route('/', name: 'app_back')]
    public function back(
        CommentRepository $commentRepository,
        Request $request,
    ): Response
    {
        return $this->render('adminBoard/index.html.twig', [
        ]);
    }

    #[Route('/Kinkster', name: 'app_back_kinkster')]
    public function kinksterAdmin(
        CommentRepository $commentRepository,
        Request $request,
    ): Response
    {
        return $this->render('adminBoard/A-Kinkster/kinkster.html.twig', [
        ]);
    }
    
}
