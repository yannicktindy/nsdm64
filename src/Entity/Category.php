<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $nom = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(length: 50)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    private ?string $descFR = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $descEn = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $presentFr = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $presentEn = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'child')]
    private ?self $parent = null;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private Collection $children;

    #[ORM\ManyToMany(targetEntity: Article::class, mappedBy: 'categories')]
    private Collection $articles;

    #[ORM\Column]
    private ?bool $isPrem = null;

    #[ORM\Column]
    private ?bool $isPrivate = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isChapter = null;

    #[ORM\ManyToMany(targetEntity: Paragraph::class, mappedBy: 'category')]
    private Collection $paragraphs;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->paragraphs = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles->add($article);
            $article->addCategory($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            $article->removeCategory($this);
        }

        return $this;
    }

    public function isIsPrem(): ?bool
    {
        return $this->isPrem;
    }

    public function setIsPrem(bool $isPrem): self
    {
        $this->isPrem = $isPrem;

        return $this;
    }

    public function isIsPrivate(): ?bool
    {
        return $this->isPrivate;
    }

    public function setIsPrivate(bool $isPrivate): self
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    public function getDescFR(): ?string
    {
        return $this->descFR;
    }

    public function setDescFR(string $descFR): self
    {
        $this->descFR = $descFR;

        return $this;
    }

    public function getDescEn(): ?string
    {
        return $this->descEn;
    }

    public function setDescEn(string $descEn): self
    {
        $this->descEn = $descEn;

        return $this;
    }

    public function getPresentFr(): ?string
    {
        return $this->presentFr;
    }

    public function setPresentFr(string $presentFr): self
    {
        $this->presentFr = $presentFr;

        return $this;
    }

    public function getPresentEn(): ?string
    {
        return $this->presentEn;
    }

    public function setPresentEn(string $presentEn): self
    {
        $this->presentEn = $presentEn;

        return $this;
    }

    public function isIsChapter(): ?bool
    {
        return $this->isChapter;
    }

    public function setIsChapter(?bool $isChapter): self
    {
        $this->isChapter = $isChapter;

        return $this;
    }

    /**
     * @return Collection<int, Paragraph>
     */
    public function getParagraphs(): Collection
    {
        return $this->paragraphs;
    }

    public function addParagraph(Paragraph $paragraph): self
    {
        if (!$this->paragraphs->contains($paragraph)) {
            $this->paragraphs->add($paragraph);
            $paragraph->addCategory($this);
        }

        return $this;
    }

    public function removeParagraph(Paragraph $paragraph): self
    {
        if ($this->paragraphs->removeElement($paragraph)) {
            $paragraph->removeCategory($this);
        }

        return $this;
    }
}
