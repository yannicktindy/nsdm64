<?php

namespace App\Entity;

use App\Repository\ImpactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ImpactRepository::class)]
class Impact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $creation = null;

    #[ORM\ManyToOne(inversedBy: 'impacts')]
    private ?Test $test = null;

    #[ORM\OneToMany(mappedBy: 'impact', targetEntity: Score::class, cascade: ['persist', 'remove'])]
    private Collection $scores;

    #[ORM\ManyToOne(inversedBy: 'impacts')]
    private ?Kinkster $kinkster = null;

    #[ORM\Column(length: 255)]
    private ?string $impactSlug = null;

    #[ORM\Column]
    private ?float $genLevel = null;

    public function __construct()
    {
        $this->scores = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->impactSlug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreation(): ?\DateTimeInterface
    {
        return $this->creation;
    }

    public function setCreation(\DateTimeInterface $creation): self
    {
        $this->creation = $creation;

        return $this;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    /**
     * @return Collection<int, Score>
     */
    public function getScores(): Collection
    {   
        // sort by score level descending
        // $scores = $this->scores->toArray();

        return $this->scores;
    }

    public function addScore(Score $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores->add($score);
            $score->setImpact($this);
        }

        return $this;
    }

    public function removeScore(Score $score): self
    {
        if ($this->scores->removeElement($score)) {
            // set the owning side to null (unless already changed)
            if ($score->getImpact() === $this) {
                $score->setImpact(null);
            }
        }

        return $this;
    }

    public function getKinkster(): ?Kinkster
    {
        return $this->kinkster;
    }

    public function setKinkster(?Kinkster $kinkster): self
    {
        $this->kinkster = $kinkster;

        return $this;
    }

    public function getImpactSlug(): ?string
    {
        return $this->impactSlug;
    }

    public function setImpactSlug(string $impactSlug): self
    {
        $this->impactSlug = $impactSlug;

        return $this;
    }

    public function getGenLevel(): ?float
    {
        return $this->genLevel;
    }

    public function setGenLevel(float $genLevel): self
    {
        $this->genLevel = $genLevel;

        return $this;
    }
}
