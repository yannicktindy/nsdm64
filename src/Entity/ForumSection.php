<?php

namespace App\Entity;

use App\Repository\ForumSectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity(repositoryClass: ForumSectionRepository::class)]
class ForumSection
{
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $titre = null;

    #[ORM\Column(type: 'text')]
    private ?string $descrFr = null;

    #[ORM\Column(type: 'integer')]
    private ?int $place = null;

    #[ORM\OneToMany(mappedBy: 'section', targetEntity: ForumRoom::class)]
    private Collection $rooms;

    #[ORM\Column(type: 'boolean')]
    private ?bool $isActive = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $isPrivate = null;

    public function __construct()
    {
        $this->rooms = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->titre;
    }
  
    public function getId(): ?int
    {
        return $this->id;
    }
    // add getter and setter for titre
    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }   

    // add getter and setter for descrFr
    public function getDescrFr(): ?string
    {
        return $this->descrFr;
    }

    public function setDescrFr(?string $descrFr): static
    {
        $this->descrFr = $descrFr;

        return $this;
    }

    // add getter and setter for place
    public function getPlace(): ?int
    {
        return $this->place;
    }

    public function setPlace(?int $place): static
    {
        $this->place = $place;

        return $this;
    }

    // add getter and setter for isActive
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): static
    {
        $this->isActive = $isActive;

        return $this;
    }

    // add getter and setter for isPrivate
    public function getIsPrivate(): ?bool
    {
        return $this->isPrivate;
    }

    public function setIsPrivate(?bool $isPrivate): static
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    /**
     * @return Collection<int, ForumRoom>
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(ForumRoom $room): static
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms->add($room);
            $room->setSection($this);
        }

        return $this;
    }

    public function removeRoom(ForumRoom $room): static
    {
        if ($this->rooms->removeElement($room)) {
            // set the owning side to null (unless already changed)
            if ($room->getSection() === $this) {
                $room->setSection(null);
            }
        }

        return $this;
    }

}
