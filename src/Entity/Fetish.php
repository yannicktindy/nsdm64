<?php

namespace App\Entity;

use App\Repository\FetishRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FetishRepository::class)]
class Fetish
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $nom = null;

    #[ORM\Column(length: 50)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'fetish', targetEntity: Kinkster::class)]
    private Collection $kinkter;

    public function __construct()
    {
        $this->kinkter = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Kinkster>
     */
    public function getKinkter(): Collection
    {
        return $this->kinkter;
    }

    public function addKinkter(Kinkster $kinkter): self
    {
        if (!$this->kinkter->contains($kinkter)) {
            $this->kinkter->add($kinkter);
            $kinkter->setFetish($this);
        }

        return $this;
    }

    public function removeKinkter(Kinkster $kinkter): self
    {
        if ($this->kinkter->removeElement($kinkter)) {
            // set the owning side to null (unless already changed)
            if ($kinkter->getFetish() === $this) {
                $kinkter->setFetish(null);
            }
        }

        return $this;
    }
}
