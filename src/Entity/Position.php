<?php

namespace App\Entity;

use App\Repository\PositionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PositionRepository::class)]
class Position
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $nom = null;

    #[ORM\Column(length: 50)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'position', targetEntity: Kinkster::class)]
    private Collection $kinksters;

    public function __construct()
    {
        $this->kinksters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Kinkster>
     */
    public function getKinksters(): Collection
    {
        return $this->kinksters;
    }

    public function addKinkster(Kinkster $kinkster): self
    {
        if (!$this->kinksters->contains($kinkster)) {
            $this->kinksters->add($kinkster);
            $kinkster->setPosition($this);
        }

        return $this;
    }

    public function removeKinkster(Kinkster $kinkster): self
    {
        if ($this->kinksters->removeElement($kinkster)) {
            // set the owning side to null (unless already changed)
            if ($kinkster->getPosition() === $this) {
                $kinkster->setPosition(null);
            }
        }

        return $this;
    }
}
