<?php

namespace App\Entity;

use App\Repository\ForumRoomRepository;
use App\Trait\DescrFrTrait;
use App\Trait\TitreTrait;
use App\Trait\BoolValidatorTrait;
use App\Trait\PlaceTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity(repositoryClass: ForumRoomRepository::class)]
class ForumRoom
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $titre = null;

    #[ORM\Column(type: 'text')]
    private ?string $descrFr = null;

    #[ORM\Column(type: 'integer')]
    private ?int $place = null;

    #[ORM\ManyToOne(inversedBy: 'rooms')]
    private ?ForumSection $section = null;

    #[ORM\OneToOne(mappedBy: 'room', cascade: ['persist', 'remove'])]
    private ?Cercle $cercle = null;

    #[ORM\OneToMany(mappedBy: 'room', targetEntity: ForumSubject::class)]
    private Collection $subjects;

    #[ORM\Column(type: 'boolean')]
    private ?bool $isActive = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $isPrivate = null;

    public function __construct()
    {
        $this->subjects = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->titre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }   

    // add getter and setter for descrFr
    public function getDescrFr(): ?string
    {
        return $this->descrFr;
    }

    public function setDescrFr(?string $descrFr): static
    {
        $this->descrFr = $descrFr;

        return $this;
    }

    // add getter and setter for place
    public function getPlace(): ?int
    {
        return $this->place;
    }

    public function setPlace(?int $place): static
    {
        $this->place = $place;

        return $this;
    }

    // add getter and setter for isActive
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): static
    {
        $this->isActive = $isActive;

        return $this;
    }

    // add getter and setter for isPrivate
    public function getIsPrivate(): ?bool
    {
        return $this->isPrivate;
    }

    public function setIsPrivate(?bool $isPrivate): static
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    public function getSection(): ?ForumSection
    {
        return $this->section;
    }

    public function setSection(?ForumSection $section): static
    {
        $this->section = $section;

        return $this;
    }

    public function getCercle(): ?Cercle
    {
        return $this->cercle;
    }

    public function setCercle(?Cercle $cercle): static
    {
        // unset the owning side of the relation if necessary
        if ($cercle === null && $this->cercle !== null) {
            $this->cercle->setRoom(null);
        }

        // set the owning side of the relation if necessary
        if ($cercle !== null && $cercle->getRoom() !== $this) {
            $cercle->setRoom($this);
        }

        $this->cercle = $cercle;

        return $this;
    }

    /**
     * @return Collection<int, ForumSubject>
     */
    public function getSubjects(): Collection
    {
        return $this->subjects;
    }

    public function addSubject(ForumSubject $subject): static
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects->add($subject);
            $subject->setRoom($this);
        }

        return $this;
    }

    public function removeSubject(ForumSubject $subject): static
    {
        if ($this->subjects->removeElement($subject)) {
            // set the owning side to null (unless already changed)
            if ($subject->getRoom() === $this) {
                $subject->setRoom(null);
            }
        }

        return $this;
    }

}
