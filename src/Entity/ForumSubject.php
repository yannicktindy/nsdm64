<?php

namespace App\Entity;

use App\Repository\ForumSubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ForumSubjectRepository::class)]
class ForumSubject
{

    /**
     * YourEntity constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTime();
        $this->messages = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->titre;
    }
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(length: 100)]
    private ?string $titre = null;

    #[ORM\Column(length: 100)]
    private ?string $slugFr = null;

    #[ORM\Column(type: 'text')]
    private ?string $descrFr = null;

    #[ORM\OneToMany(mappedBy: 'subject', targetEntity: ForumMsg::class)]
    private Collection $messages;

    #[ORM\ManyToOne(inversedBy: 'subjects')]
    private ?ForumRoom $room = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $isActive = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $isPrivate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

        /**
     * Get the value of createdAt
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     */
    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @return  self
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }  
    
    // Getter for $slugFr
    public function getSlugFr(): ?string
    {
        return $this->slugFr;
    }

    // Setter for $slugFr
    public function setSlugFr(string $slugFr): self
    {
        $this->slugFr = $slugFr;
        return $this;
    }

    // add getter and setter for descrFr
    public function getDescrFr(): ?string
    {
        return $this->descrFr;
    }

    public function setDescrFr(?string $descrFr): static
    {
        $this->descrFr = $descrFr;

        return $this;
    }

    /**
     * @return Collection<int, ForumMsg>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(ForumMsg $message): static
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
            $message->setSubject($this);
        }

        return $this;
    }

    public function removeMessage(ForumMsg $message): static
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getSubject() === $this) {
                $message->setSubject(null);
            }
        }

        return $this;
    }

    public function getRoom(): ?ForumRoom
    {
        return $this->room;
    }

    public function setRoom(?ForumRoom $room): static
    {
        $this->room = $room;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): static
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsPrivate(): ?bool
    {
        return $this->isPrivate;
    }

    public function setIsPrivate(?bool $isPrivate): static
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

}
