<?php

namespace App\Entity;

use App\Repository\ForumMsgRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ForumMsgRepository::class)]
class ForumMsg
{


    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $titre = null;

    #[ORM\Column(type: 'text')]
    private ?string $contenu = null;

    #[ORM\ManyToOne(inversedBy: 'forumMsgs')]
    private ?Kinkster $kinkster = null;

    #[ORM\ManyToOne(inversedBy: 'messages')]
    private ?ForumSubject $subject = null;
    
    #[ORM\Column]
    private ?bool $isPin = null;

    /**
     * YourEntity constructor.
     */
    public function __construct()
    {
        // $this->createdAt = new \DateTimeImmutable();
        // $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->titre ?? '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

            /**
     * Get the value of createdAt
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     */
    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @return  self
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }  
    
    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;
        return $this;
    }

    public function isIsPin(): ?bool
    {
        return $this->isPin;
    }

    public function setIsPin(bool $isPin): static
    {
        $this->isPin = $isPin;

        return $this;
    }

    public function getKinkster(): ?Kinkster
    {
        return $this->kinkster;
    }

    public function setKinkster(?Kinkster $kinkster): static
    {
        $this->kinkster = $kinkster;

        return $this;
    }

    public function getSubject(): ?ForumSubject
    {
        return $this->subject;
    }

    public function setSubject(?ForumSubject $subject): static
    {
        $this->subject = $subject;

        return $this;
    }
}
