<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Nullable;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?int $value = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $msg = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $answ = null;

    #[ORM\Column]
    private ?bool $isValid = null;

    #[ORM\Column]
    private ?bool $toKeep = null;

    #[ORM\ManyToOne(inversedBy: 'comments')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Test $test = null;

    #[ORM\ManyToOne(inversedBy: 'comments ')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Article $article = null;

    #[ORM\ManyToOne(inversedBy: 'comments')]
    private ?Kinkster $user = null;

    #[ORM\Column]
    private ?bool $isAnsw = null;

    #[ORM\Column]
    private ?bool $isArticle = null;

    #[ORM\Column]
    private ?bool $isTest = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $answAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getMsg(): ?string
    {
        return $this->msg;
    }

    public function setMsg(?string $msg): self
    {
        $this->msg = $msg;

        return $this;
    }

    public function getAnsw(): ?string
    {
        return $this->answ;
    }

    public function setAnsw(?string $answ): self
    {
        $this->answ = $answ;

        return $this;
    }

    public function isIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function isToKeep(): ?bool
    {
        return $this->toKeep;
    }

    public function setToKeep(bool $toKeep): self
    {
        $this->toKeep = $toKeep;

        return $this;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getUser(): ?Kinkster
    {
        return $this->user;
    }

    public function setUser(?Kinkster $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function isIsAnsw(): ?bool
    {
        return $this->isAnsw;
    }

    public function setIsAnsw(bool $isAnsw): self
    {
        $this->isAnsw = $isAnsw;

        return $this;
    }

    public function isIsArticle(): ?bool
    {
        return $this->isArticle;
    }

    public function setIsArticle(?bool $isArticle): self
    {
        $this->isArticle = $isArticle;

        return $this;
    }

    public function isIsTest(): ?bool
    {
        return $this->isTest;
    }

    public function setIsTest(?bool $isTest): self
    {
        $this->isTest = $isTest;

        return $this;
    }

    public function getAnswAt(): ?\DateTimeInterface
    {
        return $this->answAt;
    }

    public function setAnswAt(?\DateTimeInterface $answAt): self
    {
        $this->answAt = $answAt;

        return $this;
    }
}
