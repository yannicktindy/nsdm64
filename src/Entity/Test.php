<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TestRepository::class)]
class Test
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(nullable: true)]
    private ?int $count = null;

    #[ORM\Column(length: 50)]
    private ?string $name = null;
    
    #[ORM\Column(length: 255)]
    private ?string $testSlug = null;
    
    #[ORM\Column(length: 255)]
    private ?string $sousTitre = null;

    #[ORM\Column(length: 255)]
    private ?string $subTitle = null;

    #[ORM\Column(length: 255)]
    private ?string $testType = null;
    
    #[ORM\Column(length: 255)]
    private ?string $descFr = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $descEng = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $presentFr = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $presentEng = null;


    #[ORM\Column(length: 255, nullable: true)]
    private ?string $tags = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image = null;
    
    #[ORM\Column]
    private ?bool $isActive = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isDom = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isSub = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isGen = null;

    #[ORM\Column]
    private ?bool $advanced = null;
    
    #[ORM\Column]
    private ?bool $isOld = null;

    #[ORM\Column]
    private ?bool $isPrem = null;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Question::class, cascade: ['persist', 'remove'])]
    private Collection $questions;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Factor::class, cascade: ['persist', 'remove'])]
    private Collection $factors;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Impact::class, cascade: ['persist', 'remove'])]
    private Collection $impacts;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Comment::class)]
    private Collection $comments;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->factors = new ArrayCollection();
        $this->impacts = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setTest($this);
        }

        return $this;
    }

    public function removeQuestion(question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getTest() === $this) {
                $question->setTest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Factor>
     */
    public function getFactors(): Collection
    {
        return $this->factors;
    }

    public function addFactor(Factor $factor): self
    {
        if (!$this->factors->contains($factor)) {
            $this->factors->add($factor);
            $factor->setTest($this);
        }

        return $this;
    }

    public function removeFactor(Factor $factor): self
    {
        if ($this->factors->removeElement($factor)) {
            // set the owning side to null (unless already changed)
            if ($factor->getTest() === $this) {
                $factor->setTest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Impact>
     */
    public function getImpacts(): Collection
    {
        return $this->impacts;
    }

    public function addImpact(Impact $impact): self
    {
        if (!$this->impacts->contains($impact)) {
            $this->impacts->add($impact);
            $impact->setTest($this);
        }

        return $this;
    }

    public function removeImpact(Impact $impact): self
    {
        if ($this->impacts->removeElement($impact)) {
            // set the owning side to null (unless already changed)
            if ($impact->getTest() === $this) {
                $impact->setTest(null);
            }
        }

        return $this;
    }

    public function getDescFr(): ?string
    {
        return $this->descFr;
    }

    public function setDescFr(string $descFr): self
    {
        $this->descFr = $descFr;

        return $this;
    }

    public function getDescEng(): ?string
    {
        return $this->descEng;
    }

    public function setDescEng(string $descEng): self
    {
        $this->descEng = $descEng;

        return $this;
    }

    public function getTestSlug(): ?string
    {
        return $this->testSlug;
    }

    public function setTestSlug(string $testSlug): self
    {
        $this->testSlug = $testSlug;

        return $this;
    }

    public function isAdvanced(): ?bool
    {
        return $this->advanced;
    }

    public function setAdvanced(bool $advanced): self
    {
        $this->advanced = $advanced;

        return $this;
    }

    public function getSubTitle(): ?string
    {
        return $this->subTitle;
    }

    public function setSubTitle(string $subTitle): self
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    public function getSousTitre(): ?string
    {
        return $this->sousTitre;
    }

    public function setSousTitre(string $sousTitre): self
    {
        $this->sousTitre = $sousTitre;

        return $this;
    }

    public function isIsOld(): ?bool
    {
        return $this->isOld;
    }

    public function setIsOld(bool $isOld): self
    {
        $this->isOld = $isOld;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function isIsDom(): ?bool
    {
        return $this->isDom;
    }

    public function setIsDom(?bool $isDom): self
    {
        $this->isDom = $isDom;

        return $this;
    }

    public function isIsSub(): ?bool
    {
        return $this->isSub;
    }

    public function setIsSub(?bool $isSub): self
    {
        $this->isSub = $isSub;

        return $this;
    }

    public function isIsGen(): ?bool
    {
        return $this->isGen;
    }

    public function setIsGen(?bool $isGen): self
    {
        $this->isGen = $isGen;

        return $this;
    }

    public function isPrem(?bool $isPrem): self
    {
        $this->isPrem = $isPrem;

        return $this;
    }

    public function setIsPrem(?bool $isPrem): self
    {
        $this->isPrem = $isPrem;

        return $this;
    }

    public function getPresentFr(): ?string
    {
        return $this->presentFr;
    }

    public function setPresentFr(?string $presentFr): self
    {
        $this->presentFr = $presentFr;

        return $this;
    }

    public function getPresentEng(): ?string
    {
        return $this->presentEng;
    }

    public function setPresentEng(?string $presentEng): self
    {
        $this->presentEng = $presentEng;

        return $this;
    }

    public function getTestType(): ?string
    {
        return $this->testType;
    }

    public function setTestType(?string $testType): self
    {
        $this->testType = $testType;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(?string $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setTest($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getTest() === $this) {
                $comment->setTest(null);
            }
        }

        return $this;
    }

}
