<?php

namespace App\Entity;

use App\Repository\CercleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CercleRepository::class)]
class Cercle
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $titre = null;

    #[ORM\Column(type: 'text')]
    private ?string $descrFr = null;

    #[ORM\ManyToMany(targetEntity: Kinkster::class, inversedBy: 'cercles')]
    private Collection $kinksters;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $template = null;

    #[ORM\OneToOne(inversedBy: 'cercle', cascade: ['persist', 'remove'])]
    private ?ForumRoom $room = null;

    public function __construct()
    {
        $this->kinksters = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->titre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }   

    // add getter and setter for descrFr
    public function getDescrFr(): ?string
    {
        return $this->descrFr;
    }

    public function setDescrFr(?string $descrFr): static
    {
        $this->descrFr = $descrFr;

        return $this;
    }

    /**
     * @return Collection<int, Kinkster>
     */
    public function getKinksters(): Collection
    {
        return $this->kinksters;
    }

    public function addKinkster(Kinkster $kinkster): static
    {
        if (!$this->kinksters->contains($kinkster)) {
            $this->kinksters->add($kinkster);
        }

        return $this;
    }

    public function removeKinkster(Kinkster $kinkster): static
    {
        $this->kinksters->removeElement($kinkster);

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(?string $template): static
    {
        $this->template = $template;

        return $this;
    }

    public function getRoom(): ?ForumRoom
    {
        return $this->room;
    }

    public function setRoom(?ForumRoom $room): static
    {
        $this->room = $room;

        return $this;
    }
}
