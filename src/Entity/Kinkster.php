<?php

namespace App\Entity;

use App\Repository\KinksterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: KinksterRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class Kinkster implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 50)]
    private ?string $pseudo = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $userSlug = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null; 

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $creation = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updateAt = null;

    #[ORM\Column]
    private ?bool $isFr = null;

    #[ORM\Column]
    private ?bool $isLetter = null;

    #[ORM\Column]
    private ?bool $isValidated = null;

    #[ORM\Column]
    private ?bool $isSuspended = null;

    #[ORM\Column]
    private ?bool $isDeleted = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isKnown = null;

    #[ORM\Column(nullable: true)]
    private ?int $visit = null;

    #[ORM\OneToMany(mappedBy: 'kinkster', targetEntity: Impact::class)]
    private Collection $impacts;

    #[ORM\ManyToOne(inversedBy: 'users')]
    private ?Avatar $avatar = null;

    #[ORM\OneToMany(mappedBy: 'Kinkster', targetEntity: Visit::class)]
    private Collection $visits;

    #[ORM\ManyToOne(inversedBy: 'kinksters')]
    private ?Position $position = null;

    #[ORM\ManyToOne(inversedBy: 'kinkter')]
    private ?Fetish $fetish = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Comment::class)]
    private Collection $comments;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $securityToken = null;

    #[ORM\ManyToMany(targetEntity: Cercle::class, mappedBy: 'kinksters')]
    private Collection $cercles;

    #[ORM\OneToMany(mappedBy: 'kinkster', targetEntity: ForumMsg::class)]
    private Collection $forumMsgs;



    // #[ORM\OneToMany(mappedBy: 'kinkster', targetEntity: TestComment::class)]
    // private Collection $testComments;


    public function __construct()
    {
        $this->impacts = new ArrayCollection();
        // $this->imageUsers = new ArrayCollection();
        // $this->rooms = new ArrayCollection();
        // $this->messagesSended = new ArrayCollection();
        // $this->testComments = new ArrayCollection();
        $this->visits = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->cercles = new ArrayCollection();
        $this->forumMsgs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getPseudo();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getCreation(): ?\DateTimeInterface
    {
        return $this->creation;
    }

    public function setCreation(\DateTimeInterface $creation): self
    {
        $this->creation = $creation;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }


    /**
     * @return Collection<int, Impact>
     */
    public function getImpacts(): Collection
    {
        return $this->impacts;
    }

    public function addImpact(Impact $impact): self
    {
        if (!$this->impacts->contains($impact)) {
            $this->impacts->add($impact);
            $impact->setKinkster($this);
        }

        return $this;
    }

    public function removeImpact(Impact $impact): self
    {
        if ($this->impacts->removeElement($impact)) {
            // set the owning side to null (unless already changed)
            if ($impact->getKinkster() === $this) {
                $impact->setKinkster(null);
            }
        }

        return $this;
    }



    public function isIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function isIsSuspended(): ?bool
    {
        return $this->isSuspended;
    }

    public function setIsSuspended(bool $isSuspended): self
    {
        $this->isSuspended = $isSuspended;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getUserSlug(): ?string
    {
        return $this->userSlug;
    }

    public function setUserSlug(string $userSlug): self
    {
        $this->userSlug = $userSlug;

        return $this;
    }

    public function isIsFr(): ?bool
    {
        return $this->isFr;
    }

    public function setIsFr(bool $isFr): self
    {
        $this->isFr = $isFr;

        return $this;
    }

    public function isIsLetter(): ?bool
    {
        return $this->isLetter;
    }

    public function setIsLetter(bool $isLetter): self
    {
        $this->isLetter = $isLetter;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVisit(): ?int
    {
        return $this->visit;
    }

    public function setVisit(?int $visit): self
    {
        $this->visit = $visit;

        return $this;
    }

    public function getAvatar(): ?Avatar
    {
        return $this->avatar;
    }

    public function setAvatar(?Avatar $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function isIsKnown(): ?bool
    {
        return $this->isKnown;
    }

    public function setIsKnown(?bool $isKnown): self
    {
        $this->isKnown = $isKnown;

        return $this;
    }

    /**
     * @return Collection<int, Visit>
     */
    public function getVisits(): Collection
    {
        return $this->visits;
    }

    public function addVisit(Visit $visit): self
    {
        if (!$this->visits->contains($visit)) {
            $this->visits->add($visit);
            $visit->setKinkster($this);
        }

        return $this;
    }

    public function removeVisit(Visit $visit): self
    {
        if ($this->visits->removeElement($visit)) {
            // set the owning side to null (unless already changed)
            if ($visit->getKinkster() === $this) {
                $visit->setKinkster(null);
            }
        }

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getFetish(): ?Fetish
    {
        return $this->fetish;
    }

    public function setFetish(?Fetish $fetish): self
    {
        $this->fetish = $fetish;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    public function getSecurityToken(): ?string
    {
        return $this->securityToken;
    }

    public function setSecurityToken(?string $securityToken): self
    {
        $this->securityToken = $securityToken;

        return $this;
    }

    /**
     * @return Collection<int, Cercle>
     */
    public function getCercles(): Collection
    {
        return $this->cercles;
    }

    public function addCercle(Cercle $cercle): static
    {
        if (!$this->cercles->contains($cercle)) {
            $this->cercles->add($cercle);
            $cercle->addKinkster($this);
        }

        return $this;
    }

    public function removeCercle(Cercle $cercle): static
    {
        if ($this->cercles->removeElement($cercle)) {
            $cercle->removeKinkster($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, ForumMsg>
     */
    public function getForumMsgs(): Collection
    {
        return $this->forumMsgs;
    }

    public function addForumMsg(ForumMsg $forumMsg): static
    {
        if (!$this->forumMsgs->contains($forumMsg)) {
            $this->forumMsgs->add($forumMsg);
            $forumMsg->setKinkster($this);
        }

        return $this;
    }

    public function removeForumMsg(ForumMsg $forumMsg): static
    {
        if ($this->forumMsgs->removeElement($forumMsg)) {
            // set the owning side to null (unless already changed)
            if ($forumMsg->getKinkster() === $this) {
                $forumMsg->setKinkster(null);
            }
        }

        return $this;
    }



}
