<?php

namespace App\Form;

use App\Entity\Kinkster;
use ContainerMkLiwkM\getKarserRecaptcha3_ValidatorService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Unique;


class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pseudo', TextType::class, [
                'label' => 'Pseudo',
                'label_attr' => ['class' => 'form-label'],
                'required' => true,
                'constraints' => [
                    new Length([
                        'min' => 4,
                        'minMessage' => '4 min',
                        // max length allowed by Symfony for security reasons
                        'max' => 12,
                        'maxMessage' => '12 max',
                    ]),
                    new NotBlank([
                        'message' => 'pseudo !',
                    ]),
                    
                ],
                'attr' => ['class' => 'form-control'],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'label_attr' => ['class' => 'form-label'],
                'required' => true,
                'constraints' => [
                    // new Unique(
                    //     [
                    //         'message' => 'This email is already used',
                    // ]),
                    new NotBlank([
                        'message' => 'password !',
                    ]),
                    new Length([
                        'min' => 12,
                        'minMessage' => 'Your email should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 254,
                        'maxMessage' => 'Your email should be at least {{ limit }} characters',
                    ]),
                ],
                'attr' => ['class' => 'form-control'],
            ]) 
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'Agree Terms',
                'label_attr' => ['class' => 'form-label'],
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
                'attr' => ['class' => 'form-check-input, marx1'],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                        'maxMessage' => 'Your password should be at least {{ limit }} characters',
                    ]),
                ],
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => [
                    'label' => 'Password', 
                    'label_attr' => ['class' => 'form-label'],
                    'attr' => ['class' => 'form-control']],
                'second_options' => [
                    'label' => 'Repeat Password', 
                    'label_attr' => ['class' => 'form-label'],
                    'attr' => ['class' => 'form-control']],
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Kinkster::class,
        ]);
    }
}
