<?php

namespace App\Form\Forum;

use App\Entity\ForumSection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;

class AddSectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        $builder
            ->add('titre', TextType::class, [
                'label' => false,
                'constraints' => [
                    new Range([
                        'min' => 0,
                        'max' => 255,
                        'notInRangeMessage' => 'La valeur doit être comprise entre {{ min }} et {{ max }}.',
                    ]),
                ],
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('descrFr', TextareaType::class, [
                'label' => false,
                'constraints' => [
                    new Length([
                        'min' => 10,
                        'max' => 400,
                        'minMessage' => 'Le message doit contenir au moins {{ limit }} caractères.',
                        'maxMessage' => 'Le message ne peut pas contenir plus de {{ limit }} caractères.'
                    ]),
                ],
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('place', IntegerType::class, [
                'label' => false,
                'constraints' => [
                    new Range([
                        'min' => 0,
                        'max' => 255,
                        'notInRangeMessage' => 'La valeur doit être comprise entre {{ min }} et {{ max }}.',
                    ]),
                ],
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => false,
                'required' => false,
                'attr' => ['class' => 'form-check-input'],
            ])
            ->add('isPrivate', CheckboxType::class, [
                'label' => false,
                'required' => false,
                'attr' => ['class' => 'form-check-input'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ForumSection::class,
        ]);
    }
}

