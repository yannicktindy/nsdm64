<?php

namespace App\Form\Admin;

use App\Entity\Test;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('testSlug', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('sousTitre', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('subTitle', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('testType', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])

            ->add('descFr', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('descEng', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('presentFr', TextareaType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('presentEng', TextareaType::class, [
                'attr' => ['class' => 'form-control']
            ])

            ->add('testType', ChoiceType::class, [
                'choices' => [
                    'Type 1' => 'type-1',
                    'Type 2' => 'type-2',
                ],
                'attr' => ['class' => 'form-control']
            ])
            ->add('image', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])

            ->add('isActive', CheckboxType::class, [
                'attr' => ['class' => 'form-check-input'],
                'required' => false,
            ])
            ->add('isDom', CheckboxType::class, [
                'attr' => ['class' => 'form-check-input'],
                'required' => false,
            ])
            ->add('isSub', CheckboxType::class, [
                'attr' => ['class' => 'form-check-input'],
                'required' => false,
            ])
            ->add('isGen', CheckboxType::class, [
                'attr' => ['class' => 'form-check-input'],
                'required' => false,
            ])
            ->add('advanced', CheckboxType::class, [
                'attr' => ['class' => 'form-check-input'],
                'required' => false,
            ])
            ->add('isOld', CheckboxType::class, [
                'attr' => ['class' => 'form-check-input'],
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Test::class,
        ]);
    }
}
