<?php

namespace App\Form;

use App\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', TextareaType::class, [
                'label' => false,
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Your description should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 400,
                        'maxMessage' => 'Your description should be at least {{ limit }} characters',
                    ]),
                    new NotBlank([
                        'message' => 'Please enter a description',
                    ]),
                ],
                'attr' => [
                    'class' => 'form-control' // Ajoutez ici les classes Bootstrap souhaitées
                ],
            ])
        ;
    }
}

