<?php

namespace App\Form;

use App\Entity\Kinkster;
use ContainerMkLiwkM\getKarserRecaptcha3_ValidatorService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Unique;
use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;


class PreRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pseudo', TextType::class, [
                'label' => 'Pseudo',
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new Length([
                        'min' => 4,
                        'minMessage' => '4 min',
                        // max length allowed by Symfony for security reasons
                        'max' => 12,
                        'maxMessage' => '12 max',
                    ]),
                    new NotBlank([
                        'message' => 'pseudo !',
                    ]),

                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    // new Unique(
                    //     [
                    //         'message' => 'This email is already used',
                    // ]),
                    new NotBlank([
                        'message' => 'password !',
                    ]),
                    new Length([
                        'min' => 12,
                        'minMessage' => 'Your email should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 254,
                        'maxMessage' => 'Your email should be at least {{ limit }} characters',
                    ]),
                ],
            ]) 
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                        'maxMessage' => 'Your password should be at least {{ limit }} characters',
                    ]),
                ],
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ])
            // ->add('recaptcha', Recaptcha3Type::class, [
            //     'constraints' => [
            //         new Recaptcha3([
            //             'message' => 'The captcha is invalid.',
            //         ]),
            //     ],
            // ])
            ;
    }

}