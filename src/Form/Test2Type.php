<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class Test2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $iteration = $options['data']['countQuests'];
        // iteration = number of questions
        for ($i = 0; $i < $iteration; $i++) {
            $builder           
                ->add('choices'.$i, ChoiceType::class, [
                    'choices' => [
                        '1' => 0,
                        '2' => 8,
                        '3' => 16,
                        '4' => 24,
                        '5' => 32,
                        '6' => 40,
                        '7' => 50,
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'required' => true,
                    'label' => false,
                    'choice_name' => null,   
                ]);
        }
    }
}