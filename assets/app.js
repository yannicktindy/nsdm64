/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
console.log('app ok');
// any CSS you import will output into a single css file (app.css in this case)
// import './styles/front/main.scss';


// import * as bootstrap from 'bootstrap'
// start the Stimulus application
// import './bootstrap';
// import './js/pageModal.js';
import './js/startColor.js';
// import './js/testShowButton.js';
// import './js/carousel.js';

// Exécuter la fonction après chargement de la page
document.addEventListener('DOMContentLoaded', load);


// import './js/test.js';
console.log('app ok');

// ------------------- pop modal ----------------------
const MODAL_DELAY = 700;
async function load() {
    await new Promise(resolve => setTimeout(resolve, MODAL_DELAY));
    openModal();
}

function openModal() {
    const modal = document.getElementById('popModal');
    modal.style.opacity = 1;
    modal.style.pointerEvents = 'auto';
    const content = document.querySelector('.modl__content');
    content.style.transform = 'translateY(0)';
}
