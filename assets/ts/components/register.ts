// Sélection des éléments nécessaires
const passwordInput = document.getElementById('password') as HTMLInputElement;
const passwordMessage = document.getElementById('msgPassword') as HTMLElement;
const repeatPasswordGroup = document.getElementById('repeatPswrdGrp') as HTMLElement;
const repeatPasswordInput = document.getElementById('passwordRepeat') as HTMLInputElement;
const repeatPasswordMessage = document.getElementById('msgRepeatPassword') as HTMLElement;
const showPasswordButton = document.getElementById('showPswrd') as HTMLElement;
const showRepeatPasswordButton = document.getElementById('showRepeatPswrd') as HTMLElement;

// Définition de la regex pour le mot de passe
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/;

// Fonction pour mettre à jour l'affichage du message de mot de passe
function updatePasswordMessage() {
  if (passwordRegex.test(passwordInput.value)) {
    passwordInput.classList.remove('is-invalid');
    passwordInput.classList.add('is-valid');
    passwordMessage.classList.remove('text-danger');
    passwordMessage.classList.add('text-success');
    repeatPasswordGroup.classList.remove('d-none');
    updateRepeatPasswordMessage();
  } else {
    passwordInput.classList.remove('is-valid');
    passwordInput.classList.add('is-invalid');
    passwordMessage.classList.remove('text-success');
    passwordMessage.classList.add('text-danger');
    repeatPasswordGroup.classList.add('d-none');
  }
}

// Fonction pour mettre à jour l'affichage du message de répétition du mot de passe
function updateRepeatPasswordMessage() {
  const password = passwordInput.value;
  const repeatPassword = repeatPasswordInput.value;

  if (password === repeatPassword && passwordRegex.test(password)) {
    repeatPasswordInput.classList.remove('is-invalid');
    repeatPasswordInput.classList.add('is-valid');
    repeatPasswordMessage.textContent = 'Valide';
    repeatPasswordMessage.classList.remove('text-danger');
    repeatPasswordMessage.classList.add('text-success');
  } else {
    repeatPasswordInput.classList.remove('is-valid');
    repeatPasswordInput.classList.add('is-invalid');
    repeatPasswordMessage.textContent = 'Répétez le mot de passe';
    repeatPasswordMessage.classList.remove('text-success');
    repeatPasswordMessage.classList.add('text-danger');
  }
}

// Écoute des événements de saisie sur les champs de mot de passe
passwordInput.addEventListener('input', updatePasswordMessage);
repeatPasswordInput.addEventListener('input', updateRepeatPasswordMessage);

// Sélection des éléments nécessaires
const pseudoInput = document.getElementById('pseudo') as HTMLInputElement;
const emailInput = document.getElementById('email') as HTMLInputElement;

// Définition des regex
const pseudoRegex = /^[a-zA-Z]{4,16}$/;
const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

// Fonction pour mettre à jour l'affichage de la validation du pseudo
function updatePseudoValidation() {
  if (pseudoRegex.test(pseudoInput.value)) {
    pseudoInput.classList.remove('is-invalid');
    pseudoInput.classList.add('is-valid');
  } else {
    pseudoInput.classList.remove('is-valid');
    pseudoInput.classList.add('is-invalid');
  }
}

// Fonction pour mettre à jour l'affichage de la validation de l'email
function updateEmailValidation() {
  if (emailRegex.test(emailInput.value)) {
    emailInput.classList.remove('is-invalid');
    emailInput.classList.add('is-valid');
  } else {
    emailInput.classList.remove('is-valid');
    emailInput.classList.add('is-invalid');
  }
}

// Écoute des événements de saisie sur les champs de pseudo et d'email
pseudoInput.addEventListener('input', updatePseudoValidation);
emailInput.addEventListener('input', updateEmailValidation);