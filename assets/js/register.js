// Sélection des éléments nécessaires
const passwordInput = document.getElementById('password');
const passwordMessage = document.getElementById('msgPassword');
const repeatPasswordGroup = document.getElementById('repeatPswrdGrp');
const repeatPasswordInput = document.getElementById('passwordRepeat');
const repeatPasswordMessage = document.getElementById('msgRepeatPassword');
const showPasswordButton = document.getElementById('showPswrd');
const showRepeatPasswordButton = document.getElementById('showRepeatPswrd');

// Définition de la regex pour le mot de passe
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/;

// Fonction pour mettre à jour l'affichage du message de mot de passe
function updatePasswordMessage() {
  if (passwordRegex.test(passwordInput.value)) {
    passwordInput.classList.remove('is-invalid');
    passwordInput.classList.add('is-valid');
    passwordMessage.classList.remove('text-danger');
    passwordMessage.classList.add('text-success');
    repeatPasswordGroup.classList.remove('d-none');
    updateRepeatPasswordMessage();
  } else {
    passwordInput.classList.remove('is-valid');
    passwordInput.classList.add('is-invalid');
    passwordMessage.classList.remove('text-success');
    passwordMessage.classList.add('text-danger');
    repeatPasswordGroup.classList.add('d-none');
  }
}

// Fonction pour mettre à jour l'affichage du message de répétition du mot de passe
function updateRepeatPasswordMessage() {
  const password = passwordInput.value;
  const repeatPassword = repeatPasswordInput.value;

  if (password === repeatPassword && passwordRegex.test(password)) {
    repeatPasswordInput.classList.remove('is-invalid');
    repeatPasswordInput.classList.add('is-valid');
    repeatPasswordMessage.textContent = 'Valide';
    repeatPasswordMessage.classList.remove('text-danger');
    repeatPasswordMessage.classList.add('text-success');
  } else {
    repeatPasswordInput.classList.remove('is-valid');
    repeatPasswordInput.classList.add('is-invalid');
    repeatPasswordMessage.textContent = 'Répétez le mot de passe';
    repeatPasswordMessage.classList.remove('text-success');
    repeatPasswordMessage.classList.add('text-danger');
  }
}

// Écoute des événements de saisie sur les champs de mot de passe
passwordInput.addEventListener('input', updatePasswordMessage);
repeatPasswordInput.addEventListener('input', updateRepeatPasswordMessage);

// Sélection des éléments nécessaires
const pseudoInput = document.getElementById('pseudo');
const emailInput = document.getElementById('email');

// Définition des regex
const pseudoRegex = /^[a-zA-Z]{4,16}$/;
const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

// Fonction pour mettre à jour l'affichage de la validation du pseudo
function updatePseudoValidation() {
  if (pseudoRegex.test(pseudoInput.value)) {
    pseudoInput.classList.remove('is-invalid');
    pseudoInput.classList.add('is-valid');
  } else {
    pseudoInput.classList.remove('is-valid');
    pseudoInput.classList.add('is-invalid');
  }
}

// Fonction pour mettre à jour l'affichage de la validation de l'email
function updateEmailValidation() {
  if (emailRegex.test(emailInput.value)) {
    emailInput.classList.remove('is-invalid');
    emailInput.classList.add('is-valid');
  } else {
    emailInput.classList.remove('is-valid');
    emailInput.classList.add('is-invalid');
  }
}

// Écoute des événements de saisie sur les champs de pseudo et d'email
pseudoInput.addEventListener('input', updatePseudoValidation);
emailInput.addEventListener('input', updateEmailValidation);





// const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/;
// const myForm = document.querySelector('#register');

// export function validateForm(form) {
//   const pseudo = form.querySelector('#pseudo');
//   const email = form.querySelector('#email');
//   const passwordOne = form.querySelector('#passwordOne');
//   const passwordTwo = form.querySelector('#passwordTwo');
//   const submitButton = form.querySelector('button[type="submit"]');

//   let isValid = true;

//   // Validation du pseudo
//   if (!pseudoRegex.test(pseudo.value)) {
//     pseudo.classList.remove('is-valid');
//     pseudo.classList.add('is-invalid');
//     isValid = false;
//   } else {
//     pseudo.classList.remove('is-invalid');
//     pseudo.classList.add('is-valid');
//   }

//   // Validation de l'email
//   if (!emailRegex.test(email.value)) {
//     email.classList.remove('is-valid');
//     email.classList.add('is-invalid');
//     isValid = false;
//   } else {
//     email.classList.remove('is-invalid');
//     email.classList.add('is-valid');
//   }

//   // Validation du mot de passe
//   if (!passwordRegex.test(passwordOne.value)) {
//     passwordOne.classList.remove('is-valid');
//     passwordOne.classList.add('is-invalid');
//     passwordTwo.classList.add('d-none');
//     isValid = false;
//   } else {
//     passwordOne.classList.remove('is-invalid');
//     passwordOne.classList.add('is-valid');
//     passwordTwo.classList.remove('d-none');
//   }

//   // Validation de la confirmation du mot de passe
//   if (passwordOne.value !== passwordTwo.value) {
//     passwordTwo.classList.remove('is-valid');
//     passwordTwo.classList.add('is-invalid');
//     isValid = false;
//   } else {
//     passwordTwo.classList.remove('is-invalid');
//     passwordTwo.classList.add('is-valid');
//   }

//   // Activation/désactivation du bouton de soumission
//   submitButton.classList.toggle('d-none', !isValid || !form.querySelector('#agree-terms').checked);

//   return isValid;
// }



// myForm.addEventListener('submit', (event) => {
//   event.preventDefault();
//   if (validateForm(myForm)) {
//     // Soumettez le formulaire si tout est valide
//     myForm.submit();
//   }
// });

// text area ---------------------------------------------------------------------------------
// Sélectionner tous les éléments nécessaires
// const formGroups = document.querySelectorAll('.form-group');

// // Fonction pour mettre à jour le compteur
// function updateCounters() {
//   formGroups.forEach((formGroup) => {
//     const textarea = formGroup.querySelector('.counterArea');
//     const counterElement = formGroup.querySelector('.compteur400');

//     if (textarea && counterElement) {
//       const remainingChars = 400 - textarea.value.length;
//       counterElement.textContent = remainingChars;

//       // Mettre à jour la classe CSS en fonction du nombre de caractères restants
//       if (remainingChars <= 10) {
//         counterElement.classList.remove('text-success');
//         counterElement.classList.add('text-danger');
//       } else {
//         counterElement.classList.remove('text-danger');
//         counterElement.classList.add('text-success');
//       }

//       // Bloquer l'ajout de nouveaux caractères si le compteur atteint 0
//       if (remainingChars <= 0) {
//         textarea.setAttribute('maxlength', textarea.value.length);
//       } else {
//         textarea.removeAttribute('maxlength');
//       }
//     }
//   });
// }

// // Écouter les événements de saisie sur les textarea avec la classe counterArea
// formGroups.forEach((formGroup) => {
//   const textarea = formGroup.querySelector('.counterArea');
//   if (textarea) {
//     textarea.addEventListener('input', updateCounters);
//   }
// });

// // Initialiser les compteurs au chargement
// updateCounters();





// // repeatpassword -----------------------------------------------------------------
// // Sélection des éléments nécessaires
// const passwordInput = document.getElementById('password');
// const passwordMessage = document.getElementById('msgPassword');
// const repeatPasswordGroup = document.getElementById('repeatPswrdGrp');
// const repeatPasswordInput = document.getElementById('passwordRepeat');
// const repeatPasswordMessage = document.getElementById('msgRepeatPassword');
// const showPasswordButton = document.getElementById('showPswrd');
// const showRepeatPasswordButton = document.getElementById('showRepeatPswrd');
// // Définition de la regex pour le mot de passe
// const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/;

// // Fonction pour mettre à jour l'affichage du message de mot de passe
// function updatePasswordMessage() {
//   if (passwordRegex.test(passwordInput.value)) {
//     passwordMessage.classList.remove('text-danger');
//     passwordMessage.classList.add('text-success');
//     repeatPasswordGroup.classList.remove('d-none');
//     updateRepeatPasswordMessage();
//   } else {
//     passwordMessage.classList.remove('text-success');
//     passwordMessage.classList.add('text-danger');
//     repeatPasswordGroup.classList.add('d-none');
//   }
// }

// // Fonction pour mettre à jour l'affichage du message de répétition du mot de passe
// function updateRepeatPasswordMessage() {
//   const password = passwordInput.value;
//   const repeatPassword = repeatPasswordInput.value;

//   if (password === repeatPassword && passwordRegex.test(password)) {
//     repeatPasswordMessage.textContent = 'Valide';
//     repeatPasswordMessage.classList.remove('text-danger');
//     repeatPasswordMessage.classList.add('text-success');
//   } else {
//     repeatPasswordMessage.textContent = 'Répétez le mot de passe';
//     repeatPasswordMessage.classList.remove('text-success');
//     repeatPasswordMessage.classList.add('text-danger');
//   }
// }

// // Écoute des événements de saisie sur les champs de mot de passe
// passwordInput.addEventListener('input', updatePasswordMessage);
// repeatPasswordInput.addEventListener('input', updateRepeatPasswordMessage);

// // Sélection des éléments nécessaires
// const pseudoInput = document.getElementById('pseudo');
// const emailInput = document.getElementById('email');

// // Définition des regex
// const pseudoRegex = /^[a-zA-Z]{4,16}$/;
// const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

// // Fonction pour mettre à jour l'affichage de la validation du pseudo
// function updatePseudoValidation() {
//   if (pseudoRegex.test(pseudoInput.value)) {
//     pseudoInput.classList.remove('is-invalid');
//     pseudoInput.classList.add('is-valid');
//   } else {
//     pseudoInput.classList.remove('is-valid');
//     pseudoInput.classList.add('is-invalid');
//   }
// }

// // Fonction pour mettre à jour l'affichage de la validation de l'email
// function updateEmailValidation() {
//   if (emailRegex.test(emailInput.value)) {
//     emailInput.classList.remove('is-invalid');
//     emailInput.classList.add('is-valid');
//   } else {
//     emailInput.classList.remove('is-valid');
//     emailInput.classList.add('is-invalid');
//   }
// }

// // Écoute des événements de saisie sur les champs de pseudo et d'email
// pseudoInput.addEventListener('input', updatePseudoValidation);
// emailInput.addEventListener('input', updateEmailValidation);






// // Fonction pour basculer entre le mode texte et le mode mot de passe
// function togglePasswordVisibility(input, button) {
//     if (input.type === 'password') {
//       input.type = 'text';
//     //   button.innerHTML = '<i class="fas fa-eye-slash"></i>';
//     } else {
//       input.type = 'password';
//     //   button.innerHTML = '<i class="fas fa-eye"></i>';
//     }
//   }
  
//   // Écoute des clics sur les boutons pour afficher/masquer les mots de passe
//   showPasswordButton.addEventListener('click', () => {
//     togglePasswordVisibility(passwordInput, showPasswordButton);
//   });
  
//   showRepeatPasswordButton.addEventListener('click', () => {
//     togglePasswordVisibility(repeatPasswordInput, showRepeatPasswordButton);
//   });
