// Select all buttons with the "tag" class
const buttons = document.querySelectorAll('.tag');

// Define a function to randomly replace a button's class with "btn-primary" for 1 second
function highlightRandomButton() {
    console.log('light tag')
  // Select a random button from the list of buttons
  const randomButton = buttons[Math.floor(Math.random() * buttons.length)];
  // Replace the "btn-outline-secondary" class with "btn-primary" on the selected button
  randomButton.classList.replace('butn-tag', 'butn-tag-active');
  // Remove the "btn-primary" class after 1 second
  setTimeout(() => {
    randomButton.classList.replace('butn-tag-active', 'butn-tag');
  }, 1000);
}

// Call the "highlightRandomButton" function every 1 second
setInterval(highlightRandomButton, 1000);