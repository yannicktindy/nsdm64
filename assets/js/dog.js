class Dog {
    constructor(dogName, dogType, dogForce, description) {
    this.dogName 	= dogName;
    this.dogType 	= dogType;
    this.dogForce 	= dogForce;
    this.description= description; 
    }
}

let dog1 = new Dog ('Chien de Maître', 'type1', 0, 'description 1');
let dog2 = new Dog ('Chien de Meute', 'type2', 0, 'description 1');
let dog3 = new Dog ('Puppy', 'type3', 0, 'description 1');
let dog4 = new Dog ('Molosse', 'type4', 0, 'description 1');
let dog5 = new Dog ('Chien de garde', 'type5', 0, 'description 1');
let dog6 = new Dog ('Chien de salon', 'type6', 0, 'description 1');
let dog7 = new Dog ('Sex Dog', 'type7', 0, 'description 1');
let dog8 = new Dog ('Asex dog', 'type8', 0, 'description 1');
let dog9 = new Dog ('Chien cérébral', 'type9', 0, 'description 1');
let dog10 = new Dog ('Vraie animal', 'type10', 0, 'description 1');
let dog11 = new Dog ('Alpha', 'type11', 0, 'description 1');
let dog12 = new Dog ('Bêta', 'type12', 0, 'description 1');
let dog13 = new Dog ('Oméga', 'type13', 0, 'description 1');
let dog14 = new Dog ('Chien solitaire', 'type14', 0, 'description 1');

let tabDog = [dog1, dog2, dog3, dog4, dog5, dog6, dog7, dog8, dog9, dog10, dog11, dog12, dog13, dog14];			

class Question {
    constructor(quest, type, force, isChecked) {
    this.quest 	= quest;
    this.type 	= type;
    this.force 	= force;
    this.isChecked = isChecked;
    }
}

let q1 = new Question ("En tant que chien, le plus important pour moi est de servir un Maître et de lui appartenir","type1", 0);
let q2 = new Question ("En tant que chien, je m’épanouis et je progresse vraiment aux pieds mon Maître","type1", 0);
let q3 = new Question ("Le plus important pour moi est d’ être dans une meute. j'ai besoin d'appartenir à un groupe","type2", 0);
let q4 = new Question ("Je m’épanouis et je progresse vraiment avec d’autres chiens","type2", 0);
let q5 = new Question ("Le plus important pour moi est de jouer. J’adore être mignon et rigolo","type3", 0);
let q6 = new Question ("Je suis un chien infantile et insouciant, le plus important pour moi est de m’amuser","type3", 0);
let q7 = new Question ("Être un chien, c’est pour moi être dans le BDSM, je veux de l’intensité","type4", 0);
let q8 = new Question ("En tant que chien, j’ai besoin qu’un dominant me fasse tenir ma place pour resentir ma vrai nature","type4", 0);
let q9 = new Question ("En tant que chien je suis très protecteur. Je sais montrer les dents","type5", 0);
let q10 = new Question ("Les autres se sentent en sécurité et protégés avec moi","type5", 0);
let q11 = new Question ("En tant que chien je me sens facilement en insécurité, j’ai besoin de me sentir entouré","type6", 0);
let q12 = new Question ("En tant que chien j’ai besoin de me sentir protégé par les autres","type6", 0);
let q13 = new Question ("Être considéré comme un chien m’excite sexuellement intensément","type7", 0);
let q14 = new Question ("Je ne peux envisager ma vie de chien sans beaucoup de sexe","type7", 0);
let q15 = new Question ("Le sexe n’a aucune importance dans ma vie de chien","type8", 0);
let q16 = new Question ("Vivre ma vie de chien n’a vraiment rien de sexuel","type8", 0);
let q17 = new Question ("J’intellectualise beaucoup ma nature de chien. C’est très difficile pour moi de lâcher prise","type9", 0);
let q18 = new Question ("En temps que chien, je me contrôle, je réfléchis attentivement à ce que je dois faire","type9", 0);
let q19 = new Question ("Être un chien est totalement naturel pour moi, je me lâche complément dans mon animalité","type10", 0);
let q20 = new Question ("J’adore m’exprimer par grognement, renifler, me frotter, lécher, je laisse facilement s’exprimer ma nature animale","type10", 0);
let q21 = new Question ("Je suis un chien très autoritaire, j’aime être écouté et qu’on m’obéisse","type11", 0);
let q22 = new Question ("J’ai un ascendant naturel sur les autres chiens ou slaves que je côtoie","type11", 0);
let q23 = new Question ("En tant que chien, je veille à ce que tout fonctionne et que rien ne manque","type12", 0);
let q24 = new Question ("En tant que chien, je suis heureux lorsque j’ai une tache à accomplir et que je me sens utile","type12", 0);
let q25 = new Question ("Les autres chiens ou slaves ont beaucoup d’autorité sur moi, je leur obéis très facilement","type13", 0);
let q26 = new Question ("En tant que chien, m’offrir à tous sans jamais dire non est tout à fait normal pour moi","type13", 0);
let q27 = new Question ("En tant que chien je suis très solitaire, j’ai tendance à m’isoler souvent","type14", 0);
let q28 = new Question ("En tant que chien je ne veux servir aucun maître ni faire parti d’une meute","type14", 0);

let tabQuest = [q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20, q21, q22, q23, q24, q25, q26, q27, q28];

    let startBtn = document.querySelector('#launchBtn');
    let myForm = document.querySelector('form');
    let myBtnValid = document.querySelector('#validBtn'); 
    
    let progressBar = document.querySelector('.progress-bar')
    let count = 0;
    let progress = 0 ;
    let stepProgress = (100 / tabQuest.length);
    let forceValue;
    let isValidated = false;
    let isFull = false;
    let titleQ=document.querySelector('#titleQuest');
    let textQ=document.querySelector('#centerText');
    let testCard=document.querySelector('.myCard')
    
    let myModal = new bootstrap.Modal(document.querySelector('#myModal'));

    startBtn.addEventListener('click', function() {
        startBtn.style.display = 'none';
        myForm.style.display = 'block';
        testCard.style.display = 'none';
        function shuffleArray(inputArray){
            inputArray.sort(()=> Math.random() - 0.5);
        }
        shuffleArray(tabQuest);
        launch(count);
    })

    function launch(count) {
        myBtnValid.style.display = 'none';
        myForm.reset();
        titleQ.textContent = `Proposition n° ${count+1}`;
        textQ.textContent = tabQuest[count].quest;

        let extend = document.querySelector('#extend');
        //myCore.classList.remove("unExp");
        extend.classList.add("extend");

        myModal = new bootstrap.Modal(document.querySelector('#myModal'))
        myModal.toggle();
        selectValue(count);
    }

    function selectValue() {
        let inputsQuest = document.querySelectorAll('input');
        inputsQuest.forEach(function(input){
            input.addEventListener('click', function() {
                forceValue = parseInt(this.value);
                myBtnValid.style.display = 'block';
            })
        })
    }

    

    myBtnValid.addEventListener('click', function(e) {
        e.preventDefault();
        tabQuest[count].force = forceValue;
        myBtnValid.style.display = 'none';
        progress = stepProgress * (count + 1)
        progressBar.style.width = `${progress}%`
        myForm.reset();
        isValidated = true;
        if  (count < tabQuest.length && isValidated === true && isFull === false ){
            count++ ;
            isValidated = false;
            if (count < tabQuest.length) {
                myModal.hide()
                launch(count, tabDog, tabQuest);
            } else if (count === tabQuest.length) {
                myForm.style.display = 'none';
                isValidated = false;
                isFull = true;
                pushInTabDog(count, tabDog, tabQuest);
            }
        }  
    })

    function pushInTabDog() {
        for(let i =0; i < tabQuest.length; i++ ) {
    
            if (tabQuest[i].type === tabDog[0].dogType) {
                tabDog[0].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[1].dogType) {
                tabDog[1].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[2].dogType) {
                tabDog[2].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[3].dogType) {
                tabDog[3].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[4].dogType) {
                tabDog[4].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[5].dogType) {
                tabDog[5].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[6].dogType) {
                tabDog[6].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[7].dogType) {
                tabDog[7].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[8].dogType) {
                tabDog[8].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[9].dogType) {
                tabDog[9].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[10].dogType) {
                tabDog[10].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[11].dogType) {
                tabDog[11].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[12].dogType) {
                tabDog[12].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[13].dogType) {
                tabDog[13].dogForce += tabQuest[i].force
                
            } else if (tabQuest[i].type === tabDog[14].dogType) {
                tabDog[14].dogForce += tabQuest[i].force
                
            } 
        }
        console.log(tabDog)
        tabDog.sort(function compare(a, b) {
            if (a.dogForce > b.dogForce)
               return -1;
            if (a.dogForce < b.dogForce )
               return 1;
            return 0;
        });
        console.log(tabDog);
        showResults();
        
    }
    
    function showResults() {
        let finalResults = document.querySelector('.results')
        let textBox = document.querySelector('.textBox')
        finalResults.style.display = 'block';
        textBox.style.display = 'none';
  
        
        let divLines = document.createElement('div')
        divLines.style.textAlign = 'center';
        for (let i = 0; i < tabDog.length; i++) {
            let resultLine = document.createElement('p');
            resultLine.textContent = `${tabDog[i].dogName} = ${tabDog[i].dogForce} %`;
    
            if (tabDog[i].dogForce < 40) {
                resultLine.classList.add('txtRed')
            } else if (tabDog[i].dogForce > 60) {
                resultLine.classList.add('txtGreen')
            } else {
                resultLine.classList.add('txtYellow')
            }
            divLines.appendChild(resultLine);
        }
        titleQ.textContent = `Vos résultats`;
        finalResults.appendChild(divLines)
    }
        
   











