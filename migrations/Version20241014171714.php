<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241014171714 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) DEFAULT NULL, slug VARCHAR(255) NOT NULL, desc_fr VARCHAR(255) NOT NULL, desc_en VARCHAR(255) DEFAULT NULL, contenu LONGTEXT NOT NULL, content LONGTEXT DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_blog TINYINT(1) NOT NULL, is_nsdm TINYINT(1) NOT NULL, is_active TINYINT(1) NOT NULL, is_prem TINYINT(1) NOT NULL, is_private TINYINT(1) NOT NULL, is_zine TINYINT(1) NOT NULL, nb_rank INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_category (article_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_53A4EDAA7294869C (article_id), INDEX IDX_53A4EDAA12469DE2 (category_id), PRIMARY KEY(article_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_tag (article_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_919694F97294869C (article_id), INDEX IDX_919694F9BAD26311 (tag_id), PRIMARY KEY(article_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_paragraph (article_id INT NOT NULL, paragraph_id INT NOT NULL, INDEX IDX_A7E6C8467294869C (article_id), INDEX IDX_A7E6C8468B50597F (paragraph_id), PRIMARY KEY(article_id, paragraph_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE avatar (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, file VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) DEFAULT NULL, slug VARCHAR(50) NOT NULL, desc_fr VARCHAR(255) NOT NULL, desc_en VARCHAR(255) DEFAULT NULL, present_fr LONGTEXT NOT NULL, present_en LONGTEXT DEFAULT NULL, is_prem TINYINT(1) NOT NULL, is_private TINYINT(1) NOT NULL, is_chapter TINYINT(1) DEFAULT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cercle (id INT AUTO_INCREMENT NOT NULL, room_id INT DEFAULT NULL, titre VARCHAR(100) NOT NULL, descr_fr LONGTEXT NOT NULL, template VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1147887854177093 (room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cercle_kinkster (cercle_id INT NOT NULL, kinkster_id INT NOT NULL, INDEX IDX_A2122C327413AB9 (cercle_id), INDEX IDX_A2122C368776098 (kinkster_id), PRIMARY KEY(cercle_id, kinkster_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, article_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, value INT DEFAULT NULL, msg LONGTEXT NOT NULL, answ LONGTEXT DEFAULT NULL, is_valid TINYINT(1) NOT NULL, to_keep TINYINT(1) NOT NULL, is_answ TINYINT(1) NOT NULL, is_article TINYINT(1) NOT NULL, is_test TINYINT(1) NOT NULL, answ_at DATETIME DEFAULT NULL, INDEX IDX_9474526C1E5D0459 (test_id), INDEX IDX_9474526C7294869C (article_id), INDEX IDX_9474526CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE factor (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, desc_fr LONGTEXT NOT NULL, desc_eng LONGTEXT NOT NULL, INDEX IDX_ED38EC001E5D0459 (test_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fetish (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forum_msg (id INT AUTO_INCREMENT NOT NULL, kinkster_id INT DEFAULT NULL, subject_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL, titre VARCHAR(100) DEFAULT NULL, contenu LONGTEXT NOT NULL, is_pin TINYINT(1) NOT NULL, INDEX IDX_85A4295268776098 (kinkster_id), INDEX IDX_85A4295223EDC87 (subject_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forum_room (id INT AUTO_INCREMENT NOT NULL, section_id INT DEFAULT NULL, titre VARCHAR(100) NOT NULL, descr_fr LONGTEXT NOT NULL, place INT NOT NULL, is_active TINYINT(1) NOT NULL, is_private TINYINT(1) NOT NULL, INDEX IDX_B17EF14CD823E37A (section_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forum_section (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(100) NOT NULL, descr_fr LONGTEXT NOT NULL, place INT NOT NULL, is_active TINYINT(1) NOT NULL, is_private TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forum_subject (id INT AUTO_INCREMENT NOT NULL, room_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL, titre VARCHAR(100) NOT NULL, slug_fr VARCHAR(100) NOT NULL, descr_fr LONGTEXT NOT NULL, is_active TINYINT(1) NOT NULL, is_private TINYINT(1) NOT NULL, INDEX IDX_A02730B54177093 (room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE impact (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, kinkster_id INT DEFAULT NULL, creation DATETIME NOT NULL, impact_slug VARCHAR(255) NOT NULL, gen_level DOUBLE PRECISION NOT NULL, INDEX IDX_C409C0071E5D0459 (test_id), INDEX IDX_C409C00768776098 (kinkster_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kinkster (id INT AUTO_INCREMENT NOT NULL, avatar_id INT DEFAULT NULL, position_id INT DEFAULT NULL, fetish_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(50) NOT NULL, user_slug VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, creation DATETIME NOT NULL, update_at DATETIME NOT NULL, is_fr TINYINT(1) NOT NULL, is_letter TINYINT(1) NOT NULL, is_validated TINYINT(1) NOT NULL, is_suspended TINYINT(1) NOT NULL, is_deleted TINYINT(1) NOT NULL, is_known TINYINT(1) DEFAULT NULL, visit INT DEFAULT NULL, security_token VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_A66202C8E7927C74 (email), UNIQUE INDEX UNIQ_A66202C8E21CD7AB (user_slug), INDEX IDX_A66202C886383B10 (avatar_id), INDEX IDX_A66202C8DD842E46 (position_id), INDEX IDX_A66202C8BF47B124 (fetish_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paragraph (id INT AUTO_INCREMENT NOT NULL, updated_at DATETIME NOT NULL, titre VARCHAR(255) NOT NULL, contenu LONGTEXT NOT NULL, nb_rank INT NOT NULL, is_active TINYINT(1) NOT NULL, is_blog TINYINT(1) NOT NULL, is_private TINYINT(1) NOT NULL, is_premium TINYINT(1) NOT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE position (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, factor_id INT DEFAULT NULL, quest_eng VARCHAR(255) NOT NULL, quest_fr VARCHAR(255) NOT NULL, INDEX IDX_B6F7494E1E5D0459 (test_id), INDEX IDX_B6F7494EBC88C1A3 (factor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE score (id INT AUTO_INCREMENT NOT NULL, impact_id INT DEFAULT NULL, factor_id INT DEFAULT NULL, level DOUBLE PRECISION NOT NULL, INDEX IDX_32993751D128BC9B (impact_id), INDEX IDX_32993751BC88C1A3 (factor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test (id INT AUTO_INCREMENT NOT NULL, updated_at DATETIME NOT NULL, count INT DEFAULT NULL, name VARCHAR(50) NOT NULL, test_slug VARCHAR(255) NOT NULL, sous_titre VARCHAR(255) NOT NULL, sub_title VARCHAR(255) NOT NULL, test_type VARCHAR(255) NOT NULL, desc_fr VARCHAR(255) NOT NULL, desc_eng VARCHAR(255) DEFAULT NULL, present_fr LONGTEXT DEFAULT NULL, present_eng LONGTEXT DEFAULT NULL, tags VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) NOT NULL, is_dom TINYINT(1) DEFAULT NULL, is_sub TINYINT(1) DEFAULT NULL, is_gen TINYINT(1) DEFAULT NULL, advanced TINYINT(1) NOT NULL, is_old TINYINT(1) NOT NULL, is_prem TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_comment (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visit (id INT AUTO_INCREMENT NOT NULL, kinkster_id INT DEFAULT NULL, visit_date DATETIME NOT NULL, INDEX IDX_437EE93968776098 (kinkster_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article_category ADD CONSTRAINT FK_53A4EDAA7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_category ADD CONSTRAINT FK_53A4EDAA12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_tag ADD CONSTRAINT FK_919694F97294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_tag ADD CONSTRAINT FK_919694F9BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_paragraph ADD CONSTRAINT FK_A7E6C8467294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_paragraph ADD CONSTRAINT FK_A7E6C8468B50597F FOREIGN KEY (paragraph_id) REFERENCES paragraph (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE cercle ADD CONSTRAINT FK_1147887854177093 FOREIGN KEY (room_id) REFERENCES forum_room (id)');
        $this->addSql('ALTER TABLE cercle_kinkster ADD CONSTRAINT FK_A2122C327413AB9 FOREIGN KEY (cercle_id) REFERENCES cercle (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cercle_kinkster ADD CONSTRAINT FK_A2122C368776098 FOREIGN KEY (kinkster_id) REFERENCES kinkster (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C1E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES kinkster (id)');
        $this->addSql('ALTER TABLE factor ADD CONSTRAINT FK_ED38EC001E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE forum_msg ADD CONSTRAINT FK_85A4295268776098 FOREIGN KEY (kinkster_id) REFERENCES kinkster (id)');
        $this->addSql('ALTER TABLE forum_msg ADD CONSTRAINT FK_85A4295223EDC87 FOREIGN KEY (subject_id) REFERENCES forum_subject (id)');
        $this->addSql('ALTER TABLE forum_room ADD CONSTRAINT FK_B17EF14CD823E37A FOREIGN KEY (section_id) REFERENCES forum_section (id)');
        $this->addSql('ALTER TABLE forum_subject ADD CONSTRAINT FK_A02730B54177093 FOREIGN KEY (room_id) REFERENCES forum_room (id)');
        $this->addSql('ALTER TABLE impact ADD CONSTRAINT FK_C409C0071E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE impact ADD CONSTRAINT FK_C409C00768776098 FOREIGN KEY (kinkster_id) REFERENCES kinkster (id)');
        $this->addSql('ALTER TABLE kinkster ADD CONSTRAINT FK_A66202C886383B10 FOREIGN KEY (avatar_id) REFERENCES avatar (id)');
        $this->addSql('ALTER TABLE kinkster ADD CONSTRAINT FK_A66202C8DD842E46 FOREIGN KEY (position_id) REFERENCES position (id)');
        $this->addSql('ALTER TABLE kinkster ADD CONSTRAINT FK_A66202C8BF47B124 FOREIGN KEY (fetish_id) REFERENCES fetish (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E1E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494EBC88C1A3 FOREIGN KEY (factor_id) REFERENCES factor (id)');
        $this->addSql('ALTER TABLE score ADD CONSTRAINT FK_32993751D128BC9B FOREIGN KEY (impact_id) REFERENCES impact (id)');
        $this->addSql('ALTER TABLE score ADD CONSTRAINT FK_32993751BC88C1A3 FOREIGN KEY (factor_id) REFERENCES factor (id)');
        $this->addSql('ALTER TABLE visit ADD CONSTRAINT FK_437EE93968776098 FOREIGN KEY (kinkster_id) REFERENCES kinkster (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_category DROP FOREIGN KEY FK_53A4EDAA7294869C');
        $this->addSql('ALTER TABLE article_category DROP FOREIGN KEY FK_53A4EDAA12469DE2');
        $this->addSql('ALTER TABLE article_tag DROP FOREIGN KEY FK_919694F97294869C');
        $this->addSql('ALTER TABLE article_tag DROP FOREIGN KEY FK_919694F9BAD26311');
        $this->addSql('ALTER TABLE article_paragraph DROP FOREIGN KEY FK_A7E6C8467294869C');
        $this->addSql('ALTER TABLE article_paragraph DROP FOREIGN KEY FK_A7E6C8468B50597F');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE cercle DROP FOREIGN KEY FK_1147887854177093');
        $this->addSql('ALTER TABLE cercle_kinkster DROP FOREIGN KEY FK_A2122C327413AB9');
        $this->addSql('ALTER TABLE cercle_kinkster DROP FOREIGN KEY FK_A2122C368776098');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C1E5D0459');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C7294869C');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA76ED395');
        $this->addSql('ALTER TABLE factor DROP FOREIGN KEY FK_ED38EC001E5D0459');
        $this->addSql('ALTER TABLE forum_msg DROP FOREIGN KEY FK_85A4295268776098');
        $this->addSql('ALTER TABLE forum_msg DROP FOREIGN KEY FK_85A4295223EDC87');
        $this->addSql('ALTER TABLE forum_room DROP FOREIGN KEY FK_B17EF14CD823E37A');
        $this->addSql('ALTER TABLE forum_subject DROP FOREIGN KEY FK_A02730B54177093');
        $this->addSql('ALTER TABLE impact DROP FOREIGN KEY FK_C409C0071E5D0459');
        $this->addSql('ALTER TABLE impact DROP FOREIGN KEY FK_C409C00768776098');
        $this->addSql('ALTER TABLE kinkster DROP FOREIGN KEY FK_A66202C886383B10');
        $this->addSql('ALTER TABLE kinkster DROP FOREIGN KEY FK_A66202C8DD842E46');
        $this->addSql('ALTER TABLE kinkster DROP FOREIGN KEY FK_A66202C8BF47B124');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494E1E5D0459');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494EBC88C1A3');
        $this->addSql('ALTER TABLE score DROP FOREIGN KEY FK_32993751D128BC9B');
        $this->addSql('ALTER TABLE score DROP FOREIGN KEY FK_32993751BC88C1A3');
        $this->addSql('ALTER TABLE visit DROP FOREIGN KEY FK_437EE93968776098');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_category');
        $this->addSql('DROP TABLE article_tag');
        $this->addSql('DROP TABLE article_paragraph');
        $this->addSql('DROP TABLE avatar');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE cercle');
        $this->addSql('DROP TABLE cercle_kinkster');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE factor');
        $this->addSql('DROP TABLE fetish');
        $this->addSql('DROP TABLE forum_msg');
        $this->addSql('DROP TABLE forum_room');
        $this->addSql('DROP TABLE forum_section');
        $this->addSql('DROP TABLE forum_subject');
        $this->addSql('DROP TABLE impact');
        $this->addSql('DROP TABLE kinkster');
        $this->addSql('DROP TABLE paragraph');
        $this->addSql('DROP TABLE position');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE score');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE test');
        $this->addSql('DROP TABLE test_comment');
        $this->addSql('DROP TABLE visit');
    }
}
